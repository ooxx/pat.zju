#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>
#include <map>
#include <algorithm>

using namespace std;

bool Compare(const pair<int, float>& p1, const pair<int, float>& p2)
{
    return p1.first>p2.first;
}

int main()
{
    ifstream cin("1009.txt");
    
    map<int, float> a,b,result;
    int K1,K2;
    int exp;
    float coe;
    cin>>K1;
    for(int i=0;i<K1;i++)
    {
        cin>>exp;
        cin>>coe;
        if(a.find(exp)!=a.end())//已存在
        {
            a[exp]+=coe;
        }
        else
        {
            a[exp]=coe;
        }
    }
    cin>>K2;
    for(int i=0;i<K2;i++)
    {
        cin>>exp;
        cin>>coe;
        if(b.find(exp)!=b.end())//已存在
        {
            b[exp]+=coe;
        }
        else
        {
            b[exp]=coe;
        }
    }
    for(map<int, float>::iterator it=a.begin();it!=a.end();it++)
    {
        for(map<int, float>::iterator jt=b.begin();jt!=b.end();jt++)
        {
            int newExp=(*it).first+(*jt).first;
            float newCoe=(*it).second*(*jt).second;
            if(result.find(newExp)!=result.end())//已存在
            {
                result[newExp]+=newCoe;
            }
            else
            {
                result[newExp]=newCoe;
            }
        }
    }
    vector< pair<int, float> > pairs;
    for(map<int, float>::reverse_iterator it=result.rbegin();it!=result.rend();it++)
    {
        if((*it).second == 0)
            continue;
        pairs.push_back(make_pair((*it).first,(*it).second));
    }
    stable_sort(pairs.begin(),pairs.end(),Compare);
    cout<<pairs.size();
    for(int i=0;i<pairs.size();i++)
    {
        cout<<" "<<pairs[i].first<<" "<<setprecision(1)<<fixed<<pairs[i].second;
    }

    return 0;
}
