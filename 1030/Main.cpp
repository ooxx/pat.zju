#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <iomanip>

#include <vector>
#include <algorithm>

#define MAXV (510)
#define INF (0x6FFFFFFF)
using namespace std;


struct Node
{
    int dist;
    int call;
    int team;
    bool known;
    vector< vector<int> > Paths;
    vector<int> Costs;

    void Clear(int i)
    {
        dist = INF;//本点到起始点的距离
        call = 0;
        team = 0;
        known = false;//一开始，所有点都未加入选取集合，标记为未知
        Paths.clear();
        Costs.clear();
    }

    void Print(int i)
    {
        cout<<setw(3)<<i;
        if(dist == INF)
        {
            cout<<"     总距离INF";
        }
        else
        {
            cout<<"     总距离"<<setw(3)<<dist;
        }

        cout<<"     路径数"<<setw(3)<<Paths.size()<<"     ";

        if(known==false)
        {
            cout<<"unknown     ";
        }
        else
        {
            cout<<"  known     ";
        }

        for(int i=0;i<Paths.size();i++)
        {
            if(i != 0)
            {
                cout<<" / ";
            }
            for(int j=0;j<Paths[i].size();j++)
            {
                if(j !=0)
                {
                    cout<<">";
                }
                cout<<Paths[i][j];
            }
            cout<<"("<<Costs[i]<<")";
        }
        cout<<endl;
    }
};

struct Edge
{
    int Length;
    int Cost;
};

Node city[MAXV];
Edge adj[MAXV][MAXV];
int N,M,S,D;

void dijkstra( int start , int end )
{
    city[start].dist = 0;
    city[start].call = city[start].team;

    //即npath=1，第一个新建path的地方
    vector<int> path;
    path.push_back(start);
    city[start].Paths.push_back(path);
    city[start].Costs.push_back(0);

    while(1)
    {
        cout<<"开始新一轮循环，当前各点情况："<<endl;
        for(int i=0;i<N;i++)
        {
            city[i].Print(i);
        }

        //在所有未知节点中，查找出总距离最近的点
        //注意没有起点之分，针对未知点
        //结束这个循环后，nearest_unknown代表总距离最近的未知节点
        //若有几个点，总距离相等，则选择其中一个，选哪个对算法无影响
        //首次执行，nearest_unknown为0
        int nearest_unknown = -1;
        int nearest = INF;
        for(int i=0;i<N;i++)
        {
            if( city[i].known == false )
            {
                if( city[i].dist < nearest )
                {
                    nearest = city[i].dist;
                    nearest_unknown = i;
                }
            }
        }

        if( nearest_unknown == -1 )
        {
            cout<<"选取集合已加入所有非孤立点，无未知点，跳出"<<endl;//特殊情况，若有点孤立，则不会加入选取集合，其dist=INF
            break;
        }
        //把距离最近的点加入到选取集合，标记为已知点
        else
        {
            cout<<"找到最近的未知点："<<endl;
            city[nearest_unknown].Print(nearest_unknown);

            city[nearest_unknown].known = true;

            /*
            //到达end后立刻结束
            if( nearest_unknown == end )
            {
            cout<<"最近的未知点为终点，直接跳出"<<endl;
            return;
            }
            */

            //遍历所有和最近加入的这个点相邻的未知点，比较它们和这个点的总距离，更新所有未知相邻点的信息
            //如果是第一次入选相邻点，就用真实距离代替INF
            cout<<".....开始更新所有未知相邻点信息....."<<endl;
            for(int i=0;i<N;i++)
            {
                //判断相邻点未知
                if( adj[nearest_unknown][i].Length != INF && city[i].known == false )
                {
                    cout<<"     找到未知相邻点："<<endl<<"     ";
                    city[i].Print(i);

                    //如果从目前这点出发到达未知点的总距离，比未知点现有的总距离短，更新最短总距离
                    if( city[nearest_unknown].dist + adj[nearest_unknown][i].Length < city[i].dist )
                    {
                        city[i].dist = city[nearest_unknown].dist + adj[nearest_unknown][i].Length;

                        //本节点（city[i]）沿用上一个节点（city[nearest_unknown]）的路径信息，只在最后添加i信息
                        city[i].Paths=city[nearest_unknown].Paths;
                        city[i].Costs=city[nearest_unknown].Costs;
                        for(int n=0;n<city[i].Paths.size();n++)
                        {
                            city[i].Paths[n].push_back(i);
                            city[i].Costs[n]+=adj[nearest_unknown][i].Cost;
                        }

                        city[i].call = city[nearest_unknown].call + city[i].team;//直接覆盖

                        cout<<"     更新未知相邻点信息，更新后："<<endl<<"     ";
                        city[i].Print(i);
                    }
                    //若相等，就累加path、更新call的最大值
                    //path是累加，在上次的nearest_unknown向周围未知点扫的时候，已经赋了上次的nearest_unknown的值
                    //本次nearest_unknown再经过这一点，总数从上次累加
                    else if( city[nearest_unknown].dist + adj[nearest_unknown][i].Length == city[i].dist )
                    {
                        //将新的、来自nearest_unknown的路径，增加i信息后，添加到这个相邻未知点上
                        for(int n=0;n<city[nearest_unknown].Paths.size();n++)
                        {
                            //第二个新建path的地方
                            vector<int> path=city[nearest_unknown].Paths[n];
                            path.push_back(i);
                            city[i].Paths.push_back(path);

                            int cost=city[nearest_unknown].Costs[n];
                            cost+=adj[nearest_unknown][i].Cost;
                            city[i].Costs.push_back(cost);
                        }
                        if( city[nearest_unknown].call + city[i].team > city[i].call )
                        {
                            city[i].call = city[nearest_unknown].call + city[i].team;//有条件覆盖
                        }

                        cout<<"     更新未知相邻点信息，更新后："<<endl<<"     ";
                        city[i].Print(i);
                    }
                }
            }//for
            cout<<".....结束更新所有未知相邻点信息....."<<endl;
        }//if
    }//while
}
int main()
{

    ifstream cin("1030.txt");

    cin >> N >> M >> S >> D;

    for(int i=0;i<N;i++)
    {
        city[i].Clear(i);
    }

    for(int i=0;i<N;i++)
    {
        for(int j=0;j<N;j++)
        {
            if( i == j )
            {
                adj[i][j].Length = 0;
            }
            else
            {
                adj[i][j].Length = INF;
            }
        }
    }

    for(int i=0;i<M;i++)
    {
        int s,t,l,c;
        cin >> s >> t >> l >> c;
        if(l < adj[s][t].Length)//考虑重边的情况
        {
            adj[s][t].Length=l;
            adj[s][t].Cost=c;
            adj[t][s].Length=l;
            adj[t][s].Cost=c;
        }
    }

    dijkstra(S,D);

    cout<<"执行Dijkstra算法后各点情况："<<endl;
    for(int i=0;i<N;i++)
    {
        city[i].Print(i);
    }

    int minCost=city[D].Costs[0];
    int minCostIndex=0;
    for(int i=1;i<city[D].Costs.size();i++)
    {
        if(city[D].Costs[i]<minCost)
        {
            minCost=city[D].Costs[i];
            minCostIndex=i;
        }
    }

    for(int j=0;j<city[D].Paths[minCostIndex].size();j++)
    {
        if(j != 0)
        {
            cout<<" ";
        }
        cout<<city[D].Paths[minCostIndex][j];
    }
    cout<<" "<<city[D].dist<<" "<<city[D].Costs[minCostIndex]<<endl;
    return 0;
}