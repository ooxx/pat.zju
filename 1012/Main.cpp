#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <iomanip>

#include <vector>
#include <map>
#include <algorithm>

using namespace std;

struct Student
{
    string ID;
    int C;
    int M;
    int E;
    int A;
    int CRank;
    int MRank;
    int ERank;
    int ARank;
};

bool CompareForC(const Student& s1, const Student& s2)
{
    return s1.C>s2.C;
}
bool CompareForM(const Student& s1, const Student& s2)
{
    return s1.M>s2.M;
}
bool CompareForE(const Student& s1, const Student& s2)
{
    return s1.E>s2.E;
}
bool CompareForA(const Student& s1, const Student& s2)
{
    return s1.A>s2.A;
}

int main()
{
    ifstream cin("1012.txt");

    int N,M;
    cin>>N>>M;
    vector<Student> students(N);
    map<string, int> maps;
    for(int i=0;i<N;i++)
    {
        cin>>students[i].ID;
        cin>>students[i].C;
        cin>>students[i].M;
        cin>>students[i].E;
        students[i].A=(students[i].C+students[i].M+students[i].E)/3+0.5;
        maps[students[i].ID]=i;
    }

    vector<Student> compare=students;
    sort(compare.begin(),compare.end(),CompareForC);
    for(int i=0;i<compare.size();i++)
    {
        string id=compare[i].ID;
        students[maps[id]].CRank=i+1;

        if(i>=1)
        {
            string _id=compare[i-1].ID;
            if(students[maps[id]].C == students[maps[_id]].C)
            {
                students[maps[id]].CRank=students[maps[_id]].CRank;
            }
        }
    }
    compare=students;
    sort(compare.begin(),compare.end(),CompareForM);
    for(int i=0;i<compare.size();i++)
    {
        string id=compare[i].ID;
        students[maps[id]].MRank=i+1;

        if(i>=1)
        {
            string _id=compare[i-1].ID;
            if(students[maps[id]].M == students[maps[_id]].M)
            {
                students[maps[id]].MRank=students[maps[_id]].MRank;
            }
        }
    }
    compare=students;
    sort(compare.begin(),compare.end(),CompareForE);
    for(int i=0;i<compare.size();i++)
    {
        string id=compare[i].ID;
        students[maps[id]].ERank=i+1;

        if(i>=1)
        {
            string _id=compare[i-1].ID;
            if(students[maps[id]].E == students[maps[_id]].E)
            {
                students[maps[id]].ERank=students[maps[_id]].ERank;
            }
        }
    }
    compare=students;
    sort(compare.begin(),compare.end(),CompareForA);
    for(int i=0;i<compare.size();i++)
    {
        string id=compare[i].ID;
        students[maps[id]].ARank=i+1;

        if(i>=1)
        {
            string _id=compare[i-1].ID;
            if(students[maps[id]].A == students[maps[_id]].A)
            {
                students[maps[id]].ARank=students[maps[_id]].ARank;
            }
        }
    }

    for(int i=0;i<M;i++)
    {
        string id;
        cin>>id;
        if(maps.find(id) == maps.end())
        {
            cout<<"N/A"<<endl;
            continue;
        }
        if(
            students[maps[id]].ARank<=students[maps[id]].CRank
            &&
            students[maps[id]].ARank<=students[maps[id]].MRank
            &&
            students[maps[id]].ARank<=students[maps[id]].ERank
          )
        {
            cout<<students[maps[id]].ARank<<" A"<<endl;
        }
        else if(
            students[maps[id]].CRank<=students[maps[id]].ARank
            &&
            students[maps[id]].CRank<=students[maps[id]].MRank
            &&
            students[maps[id]].CRank<=students[maps[id]].ERank
            )
        {
            cout<<students[maps[id]].CRank<<" C"<<endl;
        }
        else if(
            students[maps[id]].MRank<=students[maps[id]].ARank
            &&
            students[maps[id]].MRank<=students[maps[id]].CRank
            &&
            students[maps[id]].MRank<=students[maps[id]].ERank
            )
        {
            cout<<students[maps[id]].MRank<<" M"<<endl;
        }
        else
        {
            cout<<students[maps[id]].ERank<<" E"<<endl;
        }
    }

    return 0;
}