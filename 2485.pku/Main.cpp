#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>

#include <string>
#include <iomanip>

#include <vector>
#include <algorithm>

#define MAXV (510)
#define INF (0x6FFFFFFF)
using namespace std;

struct Node
{
    int dist;
    bool known;
    vector< vector<int> > Paths;

    void Clear(int i)
    {
        dist = INF;//本点到起始点的距离
        known = false;//一开始，所有点都未加入选取集合，标记为未知
        Paths.clear();
    }

    void Print(int i)
    {
        cout<<setw(3)<<i;
        if(dist == INF)
        {
            cout<<"     总距离INF";
        }
        else
        {
            cout<<"     总距离"<<setw(3)<<dist;
        }

        cout<<"     路径数"<<setw(3)<<Paths.size()<<"     ";

        if(known==false)
        {
            cout<<"unknown     ";
        }
        else
        {
            cout<<"  known     ";
        }

        for(int i=0;i<Paths.size();i++)
        {
            if(i != 0)
            {
                cout<<" / ";
            }
            for(int j=0;j<Paths[i].size();j++)
            {
                if(j !=0)
                {
                    cout<<">";
                }
                cout<<Paths[i][j];
            }

        }
        cout<<endl;
    }
};

Node city[MAXV];
int adj[MAXV][MAXV];
int N,M;

int Prim(int start)
{
    int result=-1;
    
    for(int i=0;i<N;i++)
    {
        city[i].dist=adj[start][i];
    }

    while(1)
    {
        //在所有未知节点中，查找出距离known点集最近的点
        //注意没有起点之分，针对未知点
        //结束这个循环后，nearest_unknown代表总距离known点集最近的未知节点
        //若有几个点，总距离相等，则选择其中一个，选哪个对算法无影响
        //首次执行，nearest_unknown为0
        int nearest_unknown = -1;
        int nearest = INF;
        for(int i=0;i<N;i++)
        {
            if(city[i].known==false && city[i].dist<nearest)
            {
                nearest = city[i].dist;
                nearest_unknown = i;
            }
        }

        if(nearest_unknown == -1)
            break;
        else
        {
            if(nearest > result)
                result=nearest;
        }

        //把距离最近的点加入到选取集合，标记为已知点
        city[nearest_unknown].known = true;

        //更新信息
        //遍历所有和最近加入的这个点相邻的未知点，比较它们和这个点的总距离，更新所有未知相邻点的信息
        //如果是第一次入选相邻点，就用真实距离代替INF
        for(int i=0;i<N;i++)
        {
            //判断相邻点未知
            if( adj[nearest_unknown][i] != INF && city[i].known == false )
            {
                //city[i].dist表示该点到所有known点间的最短距离
                //如果发现因为nearest_unknown的新加入，使city[i]出现了更短的、到所有known点间的距离
                //则更新city[i].dist
                //具体做法，只要将city[i].dist和 i到nearest_unknown间距离比较
                if(adj[nearest_unknown][i] < city[i].dist )
                {
                    city[i].dist=adj[nearest_unknown][i];
                }
            }
        }//for
    }//while
    return result;
}
int main()
{

    freopen("2485.pku.txt","r",stdin);//"ifstream cin("2485.pku.txt");

    scanf("%d",&M);//cin>>M;
    while(M--)
    {
        cin>>N;
        for(int i=0;i<N;i++)
        {
            city[i].Clear(i);
        }
        for(int i=0;i<N;i++)
        {
            for(int j=0;j<N;j++)
            {
                scanf("%d",&adj[i][j]);//cin>>adj[i][j];
            }
        }
        printf("%d\n",Prim(0));//cout<<Prim(0)<<endl;
    }
    return 0;
}