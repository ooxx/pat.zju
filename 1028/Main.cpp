#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>

using namespace std;

struct Line
{
    int ID;
    string Name;
    int Grade;
};

bool IDCompare(const Line& l1, const Line& l2)
{
    return l1.ID<l2.ID;
}

bool NameCompare(const Line& l1, const Line& l2)
{
    if(l1.Name != l2.Name)
        return l1.Name<l2.Name;
    else
        return l1.ID<l2.ID;
}

bool GradeCompare(const Line& l1, const Line& l2)
{
    if(l1.Grade != l2.Grade)
        return l1.Grade<l2.Grade;
    else
        return l1.ID<l2.ID;
}

int main()
{
    //ifstream cin("1028.txt");
    freopen("1028.txt", "r", stdin);

    int N,C;
    scanf("%d %d",&N,&C);//cin>>N>>C;
    vector<Line> lines(N);
    for(int i=0;i<N;i++)
    {
        char temp[10];
        scanf("%d %s %d", &lines[i].ID,temp,&lines[i].Grade);
        lines[i].Name=temp;
        //cin>>lines[i].ID;
        //cin>>lines[i].Name;
        //cin>>lines[i].Grade;
    }
    switch(C)
    {
    case 1:
        sort(lines.begin(),lines.end(),IDCompare);
        break;
    case 2:
        sort(lines.begin(),lines.end(),NameCompare);
        break;
    case 3:
        sort(lines.begin(),lines.end(),GradeCompare);
        break;
    }
    for(int i=0;i<lines.size();i++)
    {
        //cout<<setw(6)<<setfill('0')<<lines[i].ID<<" "<<lines[i].Name<<" "<<lines[i].Grade<<endl;
        const char* temp=lines[i].Name.c_str();
        printf("%0.6d %s %d\n",lines[i].ID,temp,lines[i].Grade);
    }

    return 0;
}