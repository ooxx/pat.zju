#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>
#include <stack>


using namespace std;

struct Node
{
    int Index;
    vector<Node*> Neighbours;
};

struct Set
{
    vector<Node*> Content;

    void Print()
    {
        cout<<"   (";
        for(int i=0;i<Content.size();i++)
        {
            if(i != 0)
            {
                cout<<" ";
            }
            cout<<Content[i]->Index;
        }
        cout<<")"<<endl;
    }
};

int DFS(vector<Node> nodes)
{
    vector<Set> sets;

    vector<bool> choosen(nodes.size(),false);

    for(int i=0;i<nodes.size();i++)
    {
        if(choosen[nodes[i].Index]==false)
        {
            cout<<"第"<<nodes[i].Index<<"点被检测出还未选择，建立新簇和新栈"<<endl;
            Set aset;
            stack<Node*> astack;

            cout<<".....栈中加入第"<<nodes[i].Index<<"点";
            choosen[nodes[i].Index]=true;
            astack.push(&nodes[i]);
            cout<<"当前栈中有"<<astack.size()<<"个元素"<<endl;

            while(1)
            {
                //检测跳出条件
                if(astack.empty() == true)
                {
                    cout<<"跳出"<<endl;
                    break;
                }

                //出栈并加入集合
                cout<<".....栈中弹出第"<<astack.top()->Index<<"点";
                Node* anode=astack.top();astack.pop();
                cout<<"当前栈中有"<<astack.size()<<"个元素"<<endl;
                cout<<".....簇中加入第"<<anode->Index<<"点";
                aset.Content.push_back(anode);
                cout<<"当前簇中内容为";aset.Print();

                //入栈所有相关的、还没被Choosen过的项
                for(int j=0;j<anode->Neighbours.size();j++)
                {
                    if(choosen[anode->Neighbours[j]->Index]==false)
                    {
                        cout<<".....栈中加入第"<<anode->Neighbours[j]->Index<<"点";
                        choosen[anode->Neighbours[j]->Index]=true;//对应于WHITE、GRAY、BLACK标识法，在入栈时改变true/false
                        astack.push(anode->Neighbours[j]);
                        cout<<"当前栈中有"<<astack.size()<<"个元素"<<endl;
                    }
                }
            }

            cout<<"簇闭合"<<"当前簇中内容为   ";aset.Print();cout<<endl;
            sets.push_back(aset);
        }
    }

    return sets.size();
}

int main()
{
    ifstream cin("1013.txt");

    int N;
    int M;
    int K;
    cin>>N>>M>>K;

    vector<Node> nodes(N);
    vector<Set> sets;

    for(int i=0;i<N;i++)
    {
        nodes[i].Index=i;
    }
    for(int i=0;i<M;i++)
    {
        int a,b;
        cin>>a>>b;
        //a--;
        //b--;
        nodes[a].Neighbours.push_back(&nodes[b]);
        nodes[b].Neighbours.push_back(&nodes[a]);
    }

    int asize=DFS(nodes);
    cout<<"Size:"<<asize<<endl;

    return 0;
}