#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <map>

using namespace std;

int main()
{
    ifstream cin("1032.txt");
    
    map<string, int> maps;
    int a,b,N;
    cin>>a;cin>>b;cin>>N;
    if(a==b && a!=-1)
    {
        cout<<a<<endl;
        return 0;
    }
    if(a==-1 || b==-1)
    {
        cout<<"-1"<<endl;
        return 0;
    }
    for(int i=0;i<N;i++)
    {
        string A;
        cin>>A;cin>>A;cin>>A;
        if(A != "-1")
        {
            if(maps.find(A) == maps.end())
            {
                maps[A]=1;
            }
            else
            {
                maps[A]++;
            }
        }
    }

    int total2=0;
    string address;
    for(map<string, int>::iterator it=maps.begin();it!=maps.end();it++)
    {
        if((*it).second == 2)
        {
            address=(*it).first;
            total2++;
        }
    }

    if(total2 == 1)
    {
        cout<<address<<endl;
    }
    else
    {
        cout<<"-1"<<endl;
    }

    return 0;
}

//------------------------------------------------------------------------------------
// 12��
//------------------------------------------------------------------------------------