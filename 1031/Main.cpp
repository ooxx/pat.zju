#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>

using namespace std;

int main()
{
    string word;

    cin>>word;
    if((word.size()+2)%3 == 0)
    {
        int length=(word.size()+2)/3;
        vector<string> words(length);
        for(int i=0;i<words.size()-1;i++)
        {
            words[i].push_back(word[i]);
            for(int j=0;j<length-2;j++)
            {
                words[i].push_back(' ');
            }
            words[i].push_back(word[word.size()-1-i]);
        }
        for(int i=0;i<length;i++)
        {
            words[words.size()-1].push_back(word[i+length-1]);
        }

        for(int i=0;i<words.size();i++)
        {
            cout<<words[i]<<endl;
        }
    }
    else if((word.size()+2)%3 == 1)
    {
        int lengthA=(word.size()+2)/3;
        int lengthB=lengthA+1;
        vector<string> words(lengthA);
        for(int i=0;i<words.size()-1;i++)
        {
            words[i].push_back(word[i]);
            for(int j=0;j<lengthB-2;j++)
            {
                words[i].push_back(' ');
            }
            words[i].push_back(word[word.size()-1-i]);
        }
        for(int i=0;i<lengthB;i++)
        {
            words[words.size()-1].push_back(word[i+lengthA-1]);
        }

        for(int i=0;i<words.size();i++)
        {
            cout<<words[i]<<endl;
        }
    }
    else
    {
        int lengthA=(word.size()+2)/3;
        int lengthB=lengthA+2;
        vector<string> words(lengthA);
        for(int i=0;i<words.size()-1;i++)
        {
            words[i].push_back(word[i]);
            for(int j=0;j<lengthB-2;j++)
            {
                words[i].push_back(' ');
            }
            words[i].push_back(word[word.size()-1-i]);
        }
        for(int i=0;i<lengthB;i++)
        {
            words[words.size()-1].push_back(word[i+lengthA-1]);
        }

        for(int i=0;i<words.size();i++)
        {
            cout<<words[i]<<endl;
        }
    }

    return 0;
}

//------------------------------------------------------------------------------------
// ͨ��
//------------------------------------------------------------------------------------