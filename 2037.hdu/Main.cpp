#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
#include <algorithm>

using namespace std;

struct P
{
    int s;
    int e;
};

bool Compare(const P& p1, const P& p2)
{
    return p1.e<p2.e;
}

int main()
{
    freopen("2037.hdu.txt","r",stdin);
    
    int n;
    while(scanf("%d",&n) && n != 0)
    {
        vector<P> ps(n);
        for(int i=0;i<n;i++)
        {
            scanf("%d %d",&ps[i].s,&ps[i].e);
        }
        sort(ps.begin(),ps.end(),Compare);

        int time=0;
        int pCount=0;
        for(int i=0;i<ps.size();i++)
        {
            if(ps[i].s>=time)
            {
                pCount++;
                time=ps[i].e;
            }
        }
        printf("%d\n",pCount);
    }

    return 0;
}