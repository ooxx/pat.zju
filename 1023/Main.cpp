#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <algorithm>

using namespace std;

int ToInt(char ch)
{
    return ch-48;
}

char ToChar(int i)
{
    return i+48;
}

int main()
{
    string in;
    string out;
    string digitIn;
    string digitOut;
    out="0000000000000000000000000";
    digitIn=digitOut="0000000000";

    cin>>in;
    if(in == "0")
    {
        cout<<"Yes"<<endl<<"0"<<endl;
        return 0;
    }
    for(int i=0;i<in.size();i++)
    {
        int digit=ToInt(in[i]);
        digitIn[digit]='1';
    }
    reverse(in.begin(),in.end());

    in.push_back('0');
    bool flag=false;
    for(int i=0;i<in.size();i++)
    {
        int doublein=ToInt(in[i])*2;
        if(flag == true)
        {
            out[i]=ToChar((doublein+1)%10);
        }
        else
        {
            out[i]=ToChar(doublein%10);
        }
        if((doublein+flag)/10 == 1)
        {
            flag=true;
        }
        else
        {
            flag=false;
        }
    }
    reverse(out.begin(),out.end());
    while(1)
    {
        if(out[0] != '0')
            break;
        out.erase(out.begin());
    }

    for(int i=0;i<out.size();i++)
    {
        int digit=ToInt(out[i]);
        digitOut[digit]='1';
    }

    if(digitIn == digitOut)
    {
        cout<<"Yes"<<endl;
    }
    else
    {
        cout<<"No"<<endl;
    }
    cout<<out<<endl;

    return 0;
}

