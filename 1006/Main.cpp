#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <iomanip>

#include <vector>

using namespace std;
struct Time
{
    int HH;
    int MM;
    int SS;
    int Value()
    {
        return HH*60*60+MM*60+SS;
    }
};
struct Record
{
    string ID;
    Time SignInTime;
    Time SignOutTime;
};

int toInt(char ch)
{
    switch(ch)
    {
    case '0':
        return 0;
    case '1':
        return 1;
    case '2':
        return 2;
    case '3':
        return 3;
    case '4':
        return 4;
    case '5':
        return 5;
    case '6':
        return 6;
    case '7':
        return 7;
    case '8':
        return 8;
    case '9':
        return 9;
    }
}

int main()
{
    ifstream cin("1006.txt");
    
    int M;
    cin>>M;
    vector<Record> records(M);
    for(int i=0;i<M;i++)
    {
        cin>>records[i].ID;
        string time;
        cin>>time;
        records[i].SignInTime.HH=toInt(time[0])*10+toInt(time[1]);
        records[i].SignInTime.MM=toInt(time[3])*10+toInt(time[4]);
        records[i].SignInTime.SS=toInt(time[6])*10+toInt(time[7]);
        cin>>time;
        records[i].SignOutTime.HH=toInt(time[0])*10+toInt(time[1]);
        records[i].SignOutTime.MM=toInt(time[3])*10+toInt(time[4]);
        records[i].SignOutTime.SS=toInt(time[6])*10+toInt(time[7]);
    }
    Record* firstIn=&records[0];
    Record* lastOut=&records[0];
    for(int i=1;i<M;i++)
    {
        if(records[i].SignInTime.Value()<firstIn->SignInTime.Value())
        {
            firstIn=&records[i];
        }
        if(records[i].SignOutTime.Value()>lastOut->SignOutTime.Value())
        {
            lastOut=&records[i];
        }
    }
    cout<<firstIn->ID<<" "<<lastOut->ID<<endl;
    return 0;
}