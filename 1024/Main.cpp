#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <iomanip>
#include <algorithm>

using namespace std;

string ToString(long long i)
{
    ostringstream o;
    o<<i;
    return o.str();
}

long long ToInt(string s)
{
    istringstream i(s);
    long long x;
    i>>x;
    return x;
}

int ToDigit(char ch)
{
    return ch-48;
}

char ToChar(int i)
{
    return i+48;
}

int main()
{
    long long N;
    int K;
    cin>>N>>K;

    if(N/10 == 0)
    {
        cout<<N<<endl<<"0"<<endl;
        return 0;
    }

    string sN,rsN;
    int times=0;
    sN=ToString(N);
    while(1)
    {
        rsN=sN;
        reverse(rsN.begin(),rsN.end());

        if(sN == rsN)
            break;
        if(times == K)
            break;

        times++;

        //准备和
        string sum;
        for(int i=0;i<sN.size();i++)
        {
            sum.push_back('0');
        }
        sum.push_back('0');

        //相加
        bool flag=false;
        for(int i=0;i<sN.size();i++)
        {
            int add=ToDigit(sN[i])+ToDigit(rsN[i]);
            if(flag == true)
            {
                sum[i]=ToChar((add+1)%10);
            }
            else
            {
                sum[i]=ToChar(add%10);
            }
            if((add+flag)/10 == 1)//注意，求进位时也要带上flag
            {
                flag=true;
            }
            else
            {
                flag=false;
            }
        }
        flag==true ? sum[sN.size()]=ToChar(1) : sum[sN.size()]=ToChar(0);

        //扫尾
        reverse(sum.begin(),sum.end());
        while(1)
        {
            if(sum[0] != '0')
                break;
            sum.erase(sum.begin());
        }
        sN=sum;
    }

    cout<<sN<<endl<<times<<endl;
    return 0;
}
