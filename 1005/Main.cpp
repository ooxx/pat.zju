#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <iomanip>

#include <vector>

using namespace std;

int toInt(char ch)
{
    switch(ch)
    {
    case '0':
        return 0;
    case '1':
        return 1;
    case '2':
        return 2;
    case '3':
        return 3;
    case '4':
        return 4;
    case '5':
        return 5;
    case '6':
        return 6;
    case '7':
        return 7;
    case '8':
        return 8;
    case '9':
        return 9;
    }
}

string toString(int i)
{
    switch(i)
    {
    case 0:
        return "zero";
    case 1:
        return "one";
    case 2:
        return "two";
    case 3:
        return "three";
    case 4:
        return "four";
    case 5:
        return "five";
    case 6:
        return "six";
    case 7:
        return "seven";
    case 8:
        return "eight";
    case 9:
        return "nine";
    }
}

int main()
{
    string in;
    cin>>in;
    int sum=0;
    for(int i=0;i<in.size();i++)
    {
        sum+=toInt(in[i]);
    }
    
    int bai=(sum%1000)/100;
    int shi=(sum%100)/10;
    int ge=sum%10;

    if(bai==0 && shi==0)
    {
        cout<<toString(ge)<<endl;
    }
    else if(bai==0)
    {
        cout<<toString(shi)<<" "<<toString(ge)<<endl;
    }
    else
    {
        cout<<toString(bai)<<" "<<toString(shi)<<" "<<toString(ge)<<endl;
    }

    return 0;
}