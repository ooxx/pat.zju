#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

struct Node
{
    int Value;
    Node* Left;
    Node* Right;
};

int N;
vector<int> Post;
vector<int> In;

int SerachRootIndex(int InBegin, int InEnd, int rootValue)
{
    for(int i=InBegin;i<=InEnd;i++)
    {
        if(In[i] == rootValue)
        {
            return i;
        }
    }
}

Node* CreateTree(int PostBegin, int PostEnd, int InBegin, int InEnd, int Level)
{
    if(PostBegin>PostEnd || InBegin>InEnd)
        return NULL;
    
    //cout<<"当前第"<<Level<<"层，加入"<<Post[PostEnd]<<endl;

    Node* root=new Node();
    root->Value=Post[PostEnd];

    int rootIndex=SerachRootIndex(InBegin,InEnd,Post[PostEnd]);
    root->Left=CreateTree(PostBegin, PostBegin+(rootIndex-1-InBegin), InBegin, rootIndex-1,Level+1);
    root->Right=CreateTree(PostBegin+(rootIndex-1-InBegin)+1, PostEnd-1, rootIndex+1, InEnd, Level+1);
    
    return root;
}

int main()
{
    ifstream cin("1020.txt");

    cin>>N;
    for(int i=0;i<N;i++)
    {
        int temp;
        cin>>temp;
        Post.push_back(temp);
    }
    for(int i=0;i<N;i++)
    {
        int temp;
        cin>>temp;
        In.push_back(temp);
    }

    Node* root=CreateTree(0,N-1,0,N-1,1);

    queue<Node*> q;
    q.push(root);
    bool firstTime=true;
    while(q.empty() != true)
    {
        if(firstTime == true)
        {
            cout<<q.front()->Value;
            firstTime=false;
        }
        else
        {
            cout<<" "<<q.front()->Value;
        }

        if(q.front()->Left != NULL)
            q.push(q.front()->Left);
        if(q.front()->Right != NULL)
            q.push(q.front()->Right);

        q.pop();
    }

    return 0;
}

//------------------------------------------------------------------------------------
// 递归的调试，用Print比断点方便
//------------------------------------------------------------------------------------