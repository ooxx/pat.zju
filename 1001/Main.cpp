#include<iostream>
#include<string>
#include<sstream>

using namespace std;

int main()
{
    int a;
    int b;
    int output;
    string display;
    cin>>a>>b;
    output=a+b;

    //转换到数组分析
    ostringstream o;
    o<<output;
    display=o.str();

    //分正负数讨论
    if(output>=0)
    {
        if(display.length()>6)
        {
            display.insert(display.end()-6,',');
            display.insert(display.end()-3,',');
        }
        else if(display.length()>3)
        {
            display.insert(display.end()-3,',');
        }
    }
    else
    {
        if(display.length()>7)
        {
            display.insert(display.end()-6,',');
            display.insert(display.end()-3,',');
        }
        else if(display.length()>4)
        {
            display.insert(display.end()-3,',');
        }
    }

    cout<<display<<endl;


    return 0;
}
