#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>
#include <map>
#include <algorithm>

using namespace std;

char ToRadixChar(int i)
{
    if(i<=9)
    {
        return i+48;
    }
    else
    {
        return i+55;
    }
}

string ToRadixFormat(int value, int radix)
{
    string result;
    while(1)
    {
        if(value<radix)
        {
            result.insert(result.begin(),ToRadixChar(value));
            break;
        }
        result.insert(result.begin(),ToRadixChar(value%radix));
        value/=radix;
    }
    if(result.size() != 2)
        result.insert(result.begin(),'0');
    return result;
}

int main()
{
    int R,G,B;
    cin>>R>>G>>B;
    cout<<"#"<<ToRadixFormat(R,13)<<ToRadixFormat(G,13)<<ToRadixFormat(B,13)<<endl;

    return 0;
}