#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>

using namespace std;

int main()
{
    int N;
    cin>>N;
    vector<int> floors(N);
    for(int i=0;i<N;i++)
    {
        cin>>floors[i];
    }
    int currentFloor=0;
    int totalTime=0;
    for(int i=0;i<N;i++)
    {
        if(floors[i]-currentFloor>0)
        {
            totalTime+=(floors[i]-currentFloor)*6;
        }
        else
        {
            totalTime+=(currentFloor-floors[i])*4;
        }
        currentFloor=floors[i];
        totalTime+=5;
    }
    cout<<totalTime<<endl;
    return 0;
}