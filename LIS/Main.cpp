#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>

using namespace std;

struct Node
{
    int Value;
    vector<Node*> List;
};

int main()
{
    ifstream cin("LIS.txt");

    int n;
    cin>>n;
    vector<Node> list(n);
    for(int i=0;i<n;i++)
    {
        cin>>list[i].Value;
    }

    list[0].List.push_back(&list[0]);
    for(int i=1;i<n;i++)
    {
        Node* longest=&list[i];
        for(int j=0;j<i;j++)
        {
            //找出之前的nodes里，值比目前node小，且长度最长的点
            if(list[i].Value>list[j].Value)
            {
                if(list[j].List.size()>longest->List.size())
                {
                    longest=&list[j];
                }
            }
        }
        list[i].List=longest->List;
        list[i].List.push_back(&list[i]);
    }

    for(int i=0;i<n;i++)
    {
        for(int j=0;j<list[i].List.size();j++)
        {
            cout<<list[i].List[j]->Value<<" ";
        }
        cout<<endl;
    }

    return 0;
}