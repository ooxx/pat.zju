#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <sstream>
#include <algorithm>

using namespace std;

struct Book
{
    int ID;
    string Title;
    string Author;
    vector<string> KeyWords;
    string Publisher;
    string PublishedYear;
};

struct SearchQuery
{
    int Type;
    string Content;
};

bool Compare(const Book &b1, const Book &b2)
{
    return b1.ID<b2.ID;
}

int main()
{
    ifstream cin("1022.txt");

    //存数据
    int N;
    cin>>N;
    vector<Book> books(N);

    for(int ibook=0;ibook<N;ibook++)
    {
        cin>>books[ibook].ID;
        cin.get();//用以忽略"\n"
        getline(cin,books[ibook].Title);//注意string的getline用法
        getline(cin,books[ibook].Author);

        string tempKeyWord;
        string tempKeyWordsLine;
        getline(cin,tempKeyWordsLine);
        istringstream tempKeyWordsStream(tempKeyWordsLine);
        while(tempKeyWordsStream>>tempKeyWord)
        {
            books[ibook].KeyWords.push_back(tempKeyWord);
        }

        getline(cin,books[ibook].Publisher);
        cin>>books[ibook].PublishedYear;
        cin.get();//用以忽略"\n"，因为下一句是cin而不是getline，所以加或不加没有影响。
    }

    //存搜索条件
    int M;
    cin>>M;
    vector<SearchQuery> searchQueries(M);

    for(int iSearchQuery=0;iSearchQuery<M;iSearchQuery++)
    {
        string temp;

        cin>>temp;
        temp.erase(temp.end()-1);
        istringstream i(temp);
        i>>searchQueries[iSearchQuery].Type;

        cin.get();//忽略":"后的空格
        getline(cin,searchQueries[iSearchQuery].Content);
    }

    //搜索并输出
    for(int iSearchQuery=0;iSearchQuery<M;iSearchQuery++)
    {
        SearchQuery sq=searchQueries[iSearchQuery];
        vector<Book> result;
        switch(sq.Type)
        {
        case 1:
            result.clear();
            for(int ibook=0;ibook<N;ibook++)
            {
                if(books[ibook].Title == sq.Content)
                {
                    result.push_back(books[ibook]);
                }
            }
            break;
        case 2:
            result.clear();
            for(int ibook=0;ibook<N;ibook++)
            {
                if(books[ibook].Author == sq.Content)
                {
                    result.push_back(books[ibook]);
                }
            }
            break;
        case 3:
            result.clear();
            for(int ibook=0;ibook<N;ibook++)
            {
                vector<string>::iterator it=find(books[ibook].KeyWords.begin(),books[ibook].KeyWords.end(),sq.Content);
                if(it != books[ibook].KeyWords.end())
                {
                    result.push_back(books[ibook]);
                }
            }
            break;
        case 4:
            result.clear();
            for(int ibook=0;ibook<N;ibook++)
            {
                if(books[ibook].Publisher == sq.Content)
                {
                    result.push_back(books[ibook]);
                }
            }
            break;
        case 5:
            result.clear();
            for(int ibook=0;ibook<N;ibook++)
            {
                if(books[ibook].PublishedYear == sq.Content)
                {
                    result.push_back(books[ibook]);
                }
            }
            break;
        }//switch

        cout<<sq.Type<<": "<<sq.Content<<endl;

        sort(result.begin(),result.end(),Compare);

        if(result.size() != 0)
        {
            for(int i=0;i<result.size();i++)
            {
                cout<<setw(7)<<setfill('0')<<result[i].ID<<endl;
            }
        }
        else
        {
            cout<<"Not Found"<<endl;
        }
    }//for query


    return 0;
}