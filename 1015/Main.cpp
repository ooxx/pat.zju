#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <iomanip>

#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

double ToInt(char ch)
{
    if(ch>='0' && ch<='9')
    {
        return (double)(ch-48);
    }
    else
    {
        return (double)(ch-87);
    }
}

vector<int> ToRadixFormat(long long value, int radix)
{
    vector<int> result;
    while(1)
    {
        if(value<radix)
        {
            result.insert(result.begin(),value);
            break;
        }
        result.insert(result.begin(),value%radix);
        value/=radix;
    }
    return result;
}

int main()
{
    ifstream cin("1015.txt");

    vector<bool> Prime(100001,true);
    Prime[1]=false;
    long long Sqrt100000=sqrt((double)100000);
    for(int i=2;i<=Sqrt100000;i++)
    {
        if(Prime[i] == true)
        {
            for(int j=i;i*j<=100000;j++)
            {
                Prime[i*j]=false;
            }
        }
    }

    while(1)
    {
        long long N;
        long long rN=0;
        double D;
        cin>>N;
        if(N<0)
            break;
        cin>>D;

        vector<int> result=ToRadixFormat(N,D);
        reverse(result.begin(),result.end());

        for(int i=0;i<result.size();i++)
        {
            rN+=result[i]*pow(D,(double)(result.size()-i-1));
        }
        if(Prime[N]==true && Prime[rN]==true)
        {
            cout<<"Yes"<<endl;
        }
        else
        {
            cout<<"No"<<endl;
        }
    }

    return 0;
}