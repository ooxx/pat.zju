#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <algorithm>
#include <iterator>

#include <vector>
#include <set>

#include <cmath>

using namespace std;

//------------------------------------------------------------------------------------
// 数据结构
//------------------------------------------------------------------------------------
struct Window
{
    bool IsBusy;
    int RemainingTime;
};

struct Customer
{
    int ArrivingTime;
    int PrecessingTime;
    int WaitingTime;
};

//------------------------------------------------------------------------------------
// 函数
//------------------------------------------------------------------------------------
bool CustomerCompare(const Customer &c1, const Customer &c2)
{
    return c1.ArrivingTime<c2.ArrivingTime;
}

int main()
{
    ifstream cin("1017.txt");

    //输入
    int N;
    int K;
    int validN=0;
    cin>>N>>K;
    vector<Customer> customers(N);
    vector<Window> windows(K);

    for(int iN=0;iN<N;iN++)
    {
        string arrivingTime;
        int processingTime;
        cin>>arrivingTime>>processingTime;
        customers[iN].ArrivingTime=
            (
                (arrivingTime[0]-48)*10
                +
                (arrivingTime[1]-48)
            )*3600
            +
            (
                (arrivingTime[3]-48)*10
                +
                (arrivingTime[4]-48)
            )*60
            +
            (
                (arrivingTime[6]-48)*10
                +
                (arrivingTime[7]-48)
            );
        customers[iN].PrecessingTime=processingTime*60;
        if(customers[iN].ArrivingTime<17*3600)
        {
            validN++;
        }
    }

    //Customer排序
    sort(customers.begin(),customers.end(),CustomerCompare);

    //初始化各数据结构
    vector<Customer*> queue;
    for(int iN=0;iN<N;iN++)
    {
        customers[iN].WaitingTime=0;
    }
    for(int iK=0;iK<K;iK++)
    {
        windows[iK].IsBusy=true;
        windows[iK].RemainingTime=8*3600;
    }

    //按时间顺序模拟
    int customerIndex=0;
    for(int time=0;time<17*3600;time++)
    {
        //Customer进入
        if(customers[customerIndex].ArrivingTime == time)
        {

            queue.push_back(&customers[customerIndex]);
            customerIndex++;
        }

        //队伍前端前进
        vector<int> freeWindowIndex;
        for(int iK=0;iK<K;iK++)//效率很低，前8小时都在浪费时间，不过最后通过了，没做优化
        {
            if(windows[iK].IsBusy == false)
            {
                freeWindowIndex.push_back(iK);
            }
        }
        for(int iFreeWindowIndex=0;iFreeWindowIndex<freeWindowIndex.size();iFreeWindowIndex++)
        {
            if(queue.size() == 0)
            {
                break;
            }
            windows[freeWindowIndex[iFreeWindowIndex]].IsBusy=true;
            windows[freeWindowIndex[iFreeWindowIndex]].RemainingTime=(*queue[0]).PrecessingTime;
            queue.erase(queue.begin());
        }

        //队伍没轮到的等待
        for(int i=0;i<queue.size();i++)
        {
            (*queue[i]).WaitingTime++;
        }

        //Window动作
        for(int iK=0;iK<K;iK++)
        {
            if(windows[iK].IsBusy == true)
            {
                windows[iK].RemainingTime--;
                if(windows[iK].RemainingTime == 0)
                {
                    windows[iK].IsBusy=false;
                }
            }
        }
    }//time

    //计算等待时间并输出
    int totalWaitingTime=0;
    for(int iN=0;iN<N;iN++)
    {
        totalWaitingTime+=customers[iN].WaitingTime;
    }
    if(validN == 0)
    {
        cout<<"0.0"<<endl;
    }
    else
    {
        cout.precision(1);
        cout<<fixed<<(float)totalWaitingTime/(float)60/(float)validN<<endl;
    }

    return 0;
}

        