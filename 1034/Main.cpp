#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>
#include <stack>
#include <map>
#include <algorithm>


using namespace std;

struct Node
{
    int Index;
    string Name;
    int Weight;
    vector<Node*> Neighbours;
};

struct Set
{
    vector<Node*> Content;

    void Print()
    {
        cout<<"   (";
        for(int i=0;i<Content.size();i++)
        {
            if(i != 0)
            {
                cout<<" ";
            }
            cout<<Content[i]->Index;
        }
        cout<<")"<<endl;
    }
};

struct Input
{
    string A;
    string B;
    int Weight;
};

struct Gang
{
    string Head;
    int Count;
    int TotalWeight;
};

bool Compare(const Gang& gangA, const Gang& gangB)
{
    return gangA.Head<gangB.Head;
}

int main()
{
    ifstream cin("1034.txt");

    int N;
    int K;
    cin>>N>>K;

    vector<Input> inputs(N);
    map<string, int> maps;
    int totalCount=0;

    for(int i=0;i<N;i++)
    {
        cin>>inputs[i].A>>inputs[i].B>>inputs[i].Weight;
        if(maps.find(inputs[i].A) == maps.end())
        {
            maps[inputs[i].A]=totalCount;
            totalCount++;
        }
        if(maps.find(inputs[i].B) ==maps.end())
        {
            maps[inputs[i].B]=totalCount;
            totalCount++;
        }
    }

    vector<Node> nodes(totalCount);
    for(int i=0;i<totalCount;i++)
    {
        nodes[i].Index=i;
        nodes[i].Weight=0;
    }

    for(int i=0;i<N;i++)
    {
        int indexA=maps[inputs[i].A];
        int indexB=maps[inputs[i].B];
        nodes[indexA].Name=inputs[i].A;
        nodes[indexB].Name=inputs[i].B;
        nodes[indexA].Weight+=inputs[i].Weight;
        nodes[indexB].Weight+=inputs[i].Weight;
        nodes[indexA].Neighbours.push_back(&nodes[indexB]);
        nodes[indexB].Neighbours.push_back(&nodes[indexA]);
    }

    vector<Set> sets;

    vector<bool> choosen(nodes.size(),false);

    for(int i=0;i<nodes.size();i++)
    {
        if(choosen[nodes[i].Index]==false)
        {
            Set aset;
            stack<Node*> astack;

            choosen[nodes[i].Index]=true;
            astack.push(&nodes[i]);

            while(1)
            {
                //检测跳出条件
                if(astack.empty() == true)
                {
                    break;
                }

                //出栈并加入集合
                Node* anode=astack.top();astack.pop();
                aset.Content.push_back(anode);

                //入栈所有相关的、还没被Choosen过的项
                for(int j=0;j<anode->Neighbours.size();j++)
                {
                    if(choosen[anode->Neighbours[j]->Index]==false)
                    {
                        choosen[anode->Neighbours[j]->Index]=true;
                        astack.push(anode->Neighbours[j]);
                    }
                }
            }
            sets.push_back(aset);
        }
    }

    int gangCount=0;
    vector<Gang> gangs;
    for(int i=0;i<sets.size();i++)
    {
        int maxWeight=sets[i].Content[0]->Weight;
        int maxWeightIndex=0;
        int totalWeight=sets[i].Content[0]->Weight;
        for(int j=1;j<sets[i].Content.size();j++)
        {
            totalWeight+=sets[i].Content[j]->Weight;
            if(sets[i].Content[j]->Weight>maxWeight)
            {
                maxWeight=sets[i].Content[j]->Weight;
                maxWeightIndex=j;
            }
        }
        totalWeight/=2;
        if(totalWeight>K && sets[i].Content.size()>=3)
        {
            gangCount++;
            Gang agang;
            agang.Head=sets[i].Content[maxWeightIndex]->Name;
            agang.Count=sets[i].Content.size();
            agang.TotalWeight=totalWeight;
            gangs.push_back(agang);
        }
    }

    sort(gangs.begin(), gangs.end(), Compare);

    cout<<gangs.size()<<endl;
    for(int i=0;i<gangs.size();i++)
    {
        cout<<gangs[i].Head<<" "<<gangs[i].Count<<endl;
    }

    return 0;
}