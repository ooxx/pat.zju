#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>

using namespace std;

struct Node
{
    int ID;
    vector<Node*> Children;
};

int main()
{
    ifstream cin("1004.txt");

    int N,M;
    cin>>N>>M;
    vector<Node> nodes(N+1);
    for(int i=1;i<=N;i++)
    {
        nodes[i].ID=i;
    }
    for(int i=0;i<M;i++)
    {
        int header,childCount;
        cin>>header>>childCount;
        for(int j=0;j<childCount;j++)
        {
            int child;
            cin>>child;
            nodes[header].Children.push_back(&nodes[child]);
        }
    }
    
    vector<Node*> levelNodes;
    vector<Node*> nextLevelNodes;
    levelNodes.push_back(&nodes[1]);
    bool firstTime=true;
    while(1)
    {
        int leafNodesCount=0;
        nextLevelNodes.clear();
        for(int i=0;i<levelNodes.size();i++)
        {
            if(levelNodes[i]->Children.size()==0)
            {
                leafNodesCount++;
            }
            else
            {

                for(int j=0;j<levelNodes[i]->Children.size();j++)
                {
                    nextLevelNodes.push_back(levelNodes[i]->Children[j]);
                }
            }
        }
        if(firstTime==true)
        {
            firstTime=false;
            cout<<leafNodesCount;
        }
        else
        {
            cout<<" "<<leafNodesCount;
        }

        if(nextLevelNodes.size()==0)
        {
            break;
        }
        levelNodes=nextLevelNodes;
    }
    return 0;
}
        