#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

struct Testee
{
    string ID;
    int LocationNumber;
    int Score;
    int LocalRank;
    int FinalRank;
};

bool Compare(const Testee& t1, const Testee& t2)
{
    if(t1.Score != t2.Score)
        return t1.Score>t2.Score;
    else
        return t1.ID<t2.ID;
}

int main()
{
    ifstream cin("1025.txt");
    
    int N;
    cin>>N;
    vector< vector<Testee> > ranklists(N);
    vector<Testee> globallist;
    for(int i=0;i<N;i++)
    {
        int K;
        cin>>K;
        for(int j=0;j<K;j++)
        {
            Testee temp;
            cin>>temp.ID;
            cin>>temp.Score;
            temp.LocationNumber=i+1;
            ranklists[i].push_back(temp);
        }
    }

    for(int i=0;i<N;i++)
    {
        sort(ranklists[i].begin(),ranklists[i].end(),Compare);
        ranklists[i][0].LocalRank=1;
        for(int j=1;j<ranklists[i].size();j++)
        {
            if(ranklists[i][j].Score < ranklists[i][j-1].Score)
            {
                ranklists[i][j].LocalRank=j+1;
            }
            else
            {
                ranklists[i][j].LocalRank=ranklists[i][j-1].LocalRank;
            }
        }
        globallist.insert(globallist.end(),ranklists[i].begin(),ranklists[i].end());
    }

    sort(globallist.begin(),globallist.end(),Compare);
    globallist[0].FinalRank=1;
    for(int i=1;i<globallist.size();i++)
    {
        if(globallist[i].Score < globallist[i-1].Score)
        {
            globallist[i].FinalRank=i+1;
        }
        else
        {
            globallist[i].FinalRank=globallist[i-1].FinalRank;
        }
    }

    cout<<globallist.size()<<endl;
    for(int i=0;i<globallist.size();i++)
    {
        cout<<globallist[i].ID<<" "<<globallist[i].FinalRank<<" "<<globallist[i].LocationNumber<<" "<<globallist[i].LocalRank<<endl;
    }

    return 0;
}