#include <iostream>
#include <sstream>
#include <fstream>

#include <iomanip>

using namespace std;

int main()
{
    ifstream cin("1011.txt");
    
    float table[3][3];
    float odds[3];
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            cin>>table[i][j];
        }
    }
    for(int i=0;i<3;i++)
    {
        if(table[i][0]>table[i][1] && table[i][0]>table[i][2])
        {
            cout<<"W ";
            odds[i]=table[i][0];
        }
        else if(table[i][1]>table[i][0] && table[i][1]>table[i][2])
        {
            cout<<"T ";
            odds[i]=table[i][1];
        }
        else
        {
            cout<<"L ";
            odds[i]=table[i][2];
        }
    }
    cout<<setprecision(2)<<fixed<<(odds[0]*odds[1]*odds[2]*0.65-1)*2<<endl;

    return 0;
}