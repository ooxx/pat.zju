#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <iomanip>

#include <vector>
#include <algorithm>

#define MAXV (510)
#define INF (0x6FFFFFFF)
using namespace std;

struct Node
{
    int dist;
    int take;
    int bikes;
    bool known;
    vector< vector<int> > Paths;
    vector<int> Takes;

    void Clear(int i)
    {
        dist = INF;//本点到起始点的距离
        take = 0;
        bikes = 0;
        known = false;//一开始，所有点都未加入选取集合，标记为未知
        Paths.clear();
        Takes.clear();
    }

    void Print(int i)
    {
        cout<<setw(3)<<i;
        if(dist == INF)
        {
            cout<<"     总距离INF";
        }
        else
        {
            cout<<"     总距离"<<setw(3)<<dist;
        }

        cout<<"     路径数"<<setw(3)<<Paths.size()<<"     ";

        if(known==false)
        {
            cout<<"unknown     ";
        }
        else
        {
            cout<<"  known     ";
        }

        for(int i=0;i<Paths.size();i++)
        {
            if(i != 0)
            {
                cout<<" / ";
            }
            for(int j=0;j<Paths[i].size();j++)
            {
                if(j !=0)
                {
                    cout<<">";
                }
                cout<<Paths[i][j];
            }

        }
        cout<<endl;
    }
};

Node city[MAXV];
int adj[MAXV][MAXV];
int CMAX,N,M,C1,C2;

/*------------------------------------------------------------------------------------
Dij思路
void dijkstra(start end)
{
    初始化city[start]数据;
    while(1)
    {
        nearest_unknown=-1
        查找所有未知点中，总距离最短的点nearest_unknown;
        if(nearest_unknown == -1)
        {
            break;
        }
        else
        {
            city[nearest_unknown].known = true;
            for(所有点i）
            {
                if(nearest_unknown和i相邻 && i未知)
                {
                    if(起点到nearest_unknown + nearest_unknown到i < 起点直接到i)
                    {
                        更新Paths;（沿用前一点的Paths，在最后加上i的信息）
                        更新call等;
                    }
                    else if(==)
                    {
                        更新Paths;（新建一条path，将来自nearest_unknown的信息拷贝添加到path上，并加上i的信息，加入Paths）
                        更新call等;
                    }
                }
            }
        }
    }
}
------------------------------------------------------------------------------------*/
void dijkstra( int start , int end )
{
    city[start].dist = 0;
    city[start].take = city[start].bikes;

    //即npath=1，第一个新建path的地方
    vector<int> path;
    path.push_back(start);
    city[start].Paths.push_back(path);

    city[start].Takes.push_back(0);

    while(1)
    {
#ifdef DEBUG
        cout<<"开始新一轮循环，当前各点情况："<<endl;
        for(int i=0;i<N;i++)
        {
            city[i].Print(i);
        }
#endif
        //在所有未知节点中，查找出总距离最近的点
        //注意没有起点之分，针对未知点
        //结束这个循环后，nearest_unknown代表总距离最近的未知节点
        //若有几个点，总距离相等，则选择其中一个，选哪个对算法无影响
        //首次执行，nearest_unknown为0
        int nearest_unknown = -1;
        int nearest = INF;
        for(int i=0;i<N;i++)
        {
            if( city[i].known == false )
            {
                if( city[i].dist < nearest )
                {
                    nearest = city[i].dist;
                    nearest_unknown = i;
                }
            }
        }

        if( nearest_unknown == -1 )
        {
#ifdef DEBUG
            cout<<"选取集合已加入所有非孤立点，无未知点，跳出"<<endl;//特殊情况，若有点孤立，则不会加入选取集合，其dist=INF
#endif
            break;
        }
        //把距离最近的点加入到选取集合，标记为已知点
        else
        {
#ifdef DEBUG
            cout<<"找到最近的未知点："<<endl;
            city[nearest_unknown].Print(nearest_unknown);
#endif
            city[nearest_unknown].known = true;

            /*
            //到达end后立刻结束
            if( nearest_unknown == end )
            {
            cout<<"最近的未知点为终点，直接跳出"<<endl;
            return;
            }
            */

            //遍历所有和最近加入的这个点相邻的未知点，比较它们和这个点的总距离，更新所有未知相邻点的信息
            //如果是第一次入选相邻点，就用真实距离代替INF
#ifdef DEBUG
            cout<<".....开始更新所有未知相邻点信息....."<<endl;
#endif
            for(int i=0;i<N;i++)
            {
                //判断相邻点未知
                if( adj[nearest_unknown][i] != INF && city[i].known == false )
                {
#ifdef DEBUG
                    cout<<"     找到未知相邻点："<<endl<<"     ";
                    city[i].Print(i);
#endif

                    //如果从目前这点出发到达未知点的总距离，比未知点现有的总距离短，更新最短总距离
                    if( city[nearest_unknown].dist + adj[nearest_unknown][i] < city[i].dist )
                    {
                        city[i].dist = city[nearest_unknown].dist + adj[nearest_unknown][i];

                        //本节点（city[i]）沿用上一个节点（city[nearest_unknown]）的路径信息，只在最后添加i信息
                        city[i].Paths=city[nearest_unknown].Paths;
                        for(int n=0;n<city[i].Paths.size();n++)
                        {
                            city[i].Paths[n].push_back(i);
                        }
                        city[i].Takes=city[nearest_unknown].Takes;
                        for(int n=0;n<city[i].Takes.size();n++)
                        {
                            city[i].Takes[n]+=city[i].bikes;
                        }

                        city[i].take = city[nearest_unknown].take + city[i].bikes;//直接覆盖
#ifdef DEBUG
                        cout<<"     更新未知相邻点信息，更新后："<<endl<<"     ";
                        city[i].Print(i);
#endif
                    }
                    //若相等，就累加path、更新call的最大值
                    //path是累加，在上次的nearest_unknown向周围未知点扫的时候，已经赋了上次的nearest_unknown的值
                    //本次nearest_unknown再经过这一点，总数从上次累加
                    else if( city[nearest_unknown].dist + adj[nearest_unknown][i] == city[i].dist )
                    {
                        //将新的、来自nearest_unknown的路径，增加i信息后，添加到这个相邻未知点上
                        for(int n=0;n<city[nearest_unknown].Paths.size();n++)
                        {
                            //第二个新建path的地方
                            vector<int> path=city[nearest_unknown].Paths[n];
                            path.push_back(i);
                            city[i].Paths.push_back(path);
                        }
                        for(int n=0;n<city[nearest_unknown].Takes.size();n++)
                        {
                            int take=city[nearest_unknown].Takes[n];
                            take+=city[i].bikes;
                            city[i].Takes.push_back(take);
                        }
                        if( city[nearest_unknown].take + city[i].bikes > city[i].take )
                        {
                            city[i].take = city[nearest_unknown].take + city[i].bikes;//有条件覆盖
                        }
#ifdef DEBUG
                        cout<<"     更新未知相邻点信息，更新后："<<endl<<"     ";
                        city[i].Print(i);
#endif
                    }
                }
            }//for
#ifdef DEBUG
            cout<<".....结束更新所有未知相邻点信息....."<<endl;
#endif
        }//if
    }//while
}
int main()
{

    ifstream cin("1018.txt");

    cin >> CMAX;
    cin >> N >> C2 >> M;
    N++;

    city[0].Clear(0);
    city[0].bikes=0;
    for(int i=1;i<N;i++)
    {
        city[i].Clear(i);
        cin>>city[i].bikes;
    }

    for(int i=0;i<N;i++)
    {
        for(int j=0;j<N;j++)
        {
            if( i == j )
            {
                adj[i][j] = 0;
            }
            else
            {
                adj[i][j] = INF;
            }
        }
    }

    for(int i=0;i<M;i++)
    {
        int s,t,l;
        cin >> s >> t >> l;
        if(l < adj[s][t])//考虑重边的情况
        {
            adj[s][t]=l;
            adj[t][s]=l;
        }
    }

    dijkstra(0,C2);
#ifdef DEBUG
    cout<<"执行Dijkstra算法后各点情况："<<endl;
    for(int i=0;i<N;i++)
    {
        city[i].Print(i);
    }
#endif
    vector<int> sentBikes;
    for(int i=0;i<city[C2].Paths.size();i++)
    {
        int totalBikesNeeded=(city[C2].Paths[i].size()-1)*CMAX/2;
        int takenFromPath=city[C2].Takes[i];
        sentBikes.push_back(totalBikesNeeded-takenFromPath);
    }

    int minSentBikes=sentBikes[0];
    int minSentBikesIndex=0;
    for(int i=0;i<sentBikes.size();i++)
    {
        if(sentBikes[i]<minSentBikes)
        {
            minSentBikes=sentBikes[i];
            minSentBikesIndex=i;
        }
    }

    cout<<minSentBikes<<" ";
    for(int i=0;i<city[C2].Paths[minSentBikesIndex].size();i++)
    {
        if(i != 0)
        {
            cout<<"->";
        }
        cout<<city[C2].Paths[minSentBikesIndex][i];
    }
    cout<<" 0"<<endl;
    return 0;
}

//------------------------------------------------------------------------------------
// 只考虑最简单的、车分配过去刚刚好、到最后0辆车返回中心的情况，得18分
//------------------------------------------------------------------------------------