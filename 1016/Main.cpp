#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>

#define ONLINE true
#define OFFLINE false

using namespace std;

int rate[24];

struct Record
{
    string Customer;
    string ToPrint;
    int Day;
    int Hour;
    int Minute;
    int Time;
    bool Type;
};

struct Pair
{
    Record StartRecord;
    Record EndRecord;
    float Cost;
};

struct Bill
{
    string Customer;
    vector<Record> Records;
    vector<Pair> RecordPairs;
    float TotalAmount;
};

bool RecordCompare (const Record &r1, const Record &r2)
{
    return r1.Time<r2.Time;
}

bool BillCompare(const Bill &b1, const Bill &b2)
{
    return b1.Customer<b2.Customer;
}

float CalculateAmount(int time)
{
    int day=time/(24*60);
    int hour=(time-day*(24*60))/60;
    int min=time-day*(24*60)-hour*60;

    float amount=0;
    for(int k=0;k<24;k++)
    {
        amount+=rate[k]*60*day;
    }
    for(int k=0;k<hour;k++)
    {
        amount+=rate[k]*60;
    }
    amount+=rate[hour]*min;

    return amount;
}


int main()
{
    ifstream cin("1016.txt");

    int month;

    //读取
    for(int i=0;i<24;i++)
    {
        cin>>rate[i];
    }

    int N;
    cin>>N;
    vector<Record> records(N);

    for(int i=0;i<N;i++)
    {
        cin>>records[i].Customer;

        string temp;
        cin>>temp;
        //转换方法待改进
        month=(temp[0]-48)*10+(temp[1]-48);
        records[i].Day=(temp[3]-48)*10+(temp[4]-48);
        records[i].Hour=(temp[6]-48)*10+(temp[7]-48);
        records[i].Minute=(temp[9]-48)*10+(temp[10]-48);
        records[i].Time=records[i].Day*24*60+records[i].Hour*60+records[i].Minute;
        temp.erase(temp.begin());
        temp.erase(temp.begin());
        temp.erase(temp.begin());
        records[i].ToPrint=temp;
        cin>>temp;
        if(temp == "on-line")
        {
            records[i].Type=ONLINE;
        }
        else
        {
            records[i].Type=OFFLINE;
        }
    }

    //分组，建立Bill。分组算法很乱，需要改进
    vector<Bill> bills(1);

    bills[0].Customer=records[0].Customer;
    bills[0].Records.push_back(records[0]);

    for(int i=1;i<N;i++)
    {
        string customer=records[i].Customer;
        int billSize=bills.size();
        bool isNew=true;
        for(int j=0;j<billSize;j++)
        {
            if(customer == bills[j].Customer)
            {
                bills[j].Records.push_back(records[i]);
                isNew=false;
            }
        }
        if(isNew==true)
        {
            Bill newBill;
            newBill.Customer=records[i].Customer;
            newBill.Records.push_back(records[i]);
            bills.push_back(newBill);
        }
    }//for records

    //每个Bill内Record按时间排序
    for(int i=0;i<bills.size();i++)
    {
        sort(bills[i].Records.begin(),bills[i].Records.end(),RecordCompare);
    }

    //移除无效数据、配对
    for(int i=0;i<bills.size();i++)
    {
        records=bills[i].Records;

        while(1)
        {
            if(records[0].Type == OFFLINE)//[0]=OFF
            {
                records.erase(records.begin());
            }
            else//[0]=ON
            {
                if(records[1].Type == OFFLINE)
                {
                    Pair pair;
                    pair.StartRecord=records[0];
                    pair.EndRecord=records[1];

                    bills[i].RecordPairs.push_back(pair);

                    records.erase(records.begin());
                    records.erase(records.begin());
                }
                else
                {
                    records.erase(records.begin());
                }
            }

            if(records.size() <= 1)
            {
                break;
            }
        }
    }

    //计算总费用
    for(int i=0;i<bills.size();i++)
    {
        float totalAmount=0;
        for(int j=0;j<bills[i].RecordPairs.size();j++)
        {
            float amount1=CalculateAmount(bills[i].RecordPairs[j].StartRecord.Time);
            float amount2=CalculateAmount(bills[i].RecordPairs[j].EndRecord.Time);
            bills[i].RecordPairs[j].Cost=amount2-amount1;
            totalAmount+=amount2-amount1;
        }
        bills[i].TotalAmount=totalAmount;
    }

    //根据Customer名字排序，为输出做准备
    sort(bills.begin(),bills.end(),BillCompare);

    //输出
    cout.precision(2);
    for(int i=0;i<bills.size();i++)
    {
        if(bills[i].RecordPairs.size() != 0)
        {
            cout<<setfill('0');
            cout<<bills[i].Customer<<" "<<setw(2)<<month<<endl;
            cout<<setfill(' ');

            for(int j=0;j<bills[i].RecordPairs.size();j++)
            {
                cout<<bills[i].RecordPairs[j].StartRecord.ToPrint<<" ";
                cout<<bills[i].RecordPairs[j].EndRecord.ToPrint<<" ";
                cout<<bills[i].RecordPairs[j].EndRecord.Time-bills[i].RecordPairs[j].StartRecord.Time<<" ";
                cout<<"$"<<fixed<<bills[i].RecordPairs[j].Cost/100<<endl;
            }
            cout<<"Total amount: $"<<bills[i].TotalAmount/100<<endl;
        }
    }

    return 0;
}

//------------------------------------------------------------------------------------
// 配对、忽略错误的信息操作中，指针移动易出错，不好计算。
// 这里用指针固定在头的办法。忽略的和已经记录的数据都移除。
//------------------------------------------------------------------------------------