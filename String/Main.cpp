#include <iostream>
#include <sstream>
#include <fstream>
#include <stdio.h>

#include <string>
#include <iomanip>//io manipulator

#include <vector>
#include <algorithm>

using namespace std;

string ToString(double x)
{
    ostringstream o;

    //o.precision(8);
    //o.fixed;

    if(o<<x)
        return o.str();
    return "error";
}

double ToDouble(const string &s)
{
    istringstream i(s);
    double x;
    if(i>>x)
        return x;
    return 0.0;
}

int ToDigit(char ch)
{
    return ch-48;
}

char ToChar(int i)
{
    return i+48;
}

int main()
{
    //C语言输入输出流控制
    freopen("String.txt","r",stdin);
    int z;
    while(scanf("%d",&z) != EOF)
    {
        printf("%d ",z);
    }
    printf("\n");
    fclose(stdin);

    //C++输入输出流控制
    ifstream cin("String.txt");
    int y;
    while(cin>>y)
    {
        cout<<y<<" ";
    }
    cout<<endl;
    cin.close();

    //String与数字的转换
    double a=3.1415926;
    string b=ToString(a);
    cout<<b<<endl;

    string c="0.618";
    double d=ToDouble(c);
    cout<<d<<endl;

    //char与数字的转换
    char e='9';
    int f=ToDigit(e);
    cout<<f<<endl;

    int g=8;
    char h=ToChar(g);
    cout<<h<<endl;

    //C语言格式化输出
    //-：结果左对齐，右边填空格
    //+：输出正负号
    //#：为%x、%o增加前缀
    //.：设定小数位数
    printf("%lld\n" , (long long)0x7fffffffffffffff);//最大的整数9223372036854775807，19位
    printf("%lld\n" , (long long)-0x7fffffffffffffff);//最小的整数-9223372036854775807，19位
    printf("\n");

    printf("*%15.7d*\n", 5555);//对于%d，x.y表示总长x，有数字的y，分别用空格和0填充
    printf("*%15.7f*\n", 3.14159);//对于%f，x.y表示总长x，小数后y位，分别用空格和0填充
    printf("\n");

    char i[20]="Hello World!";
    int j=20;
    int k=11;
    printf("%*.*s\n",j,k,i);

    printf("%d\n" , 223);
    printf("%o\n" , 223);
    printf("%x\n" , 223);
    printf("\n");

    printf("%e\n" , 232.11111111);
    printf("%e\n" , -232.11111111);
    printf("\n");

    printf("%c\n" , 'a');
    printf("%c\n" , 97);
    printf("%s\n" , "this is a test!");
    printf("\n");

    printf("*%-10d*\n", 223);
    printf("*%+10d*\n", 232);
    printf("*%+10d*\n", -232);
    printf("\n");

    printf("*%#o*\n", 232);
    printf("*%#x*\n", 232);
    printf("\n");

    //C++格式化输出
    cout<<31.415926<<endl;

    cout<<setw(3)<<1<<endl;//setw短时间作用
    cout.width(10);//width同样是短时间作用
    cout<<setbase(8)<<255<<endl;//setbase短时间作用
    cout<<setbase(16)<<255<<endl;
    cout<<31.415926<<endl;

    cout<<fixed<<3.1415926<<endl;//fixed长期作用
    cout<<resetiosflags(ios::fixed);

    cout<<scientific<<31.1415926<<endl;//scientific长期作用
    cout<<resetiosflags(ios::scientific);

    cout<<uppercase<<setbase(16)<<0xffff<<endl;//uppercase长期作用
    cout<<resetiosflags(ios::uppercase);

    cout<<left<<setw(3)<<1<<endl;//left长期作用
    cout.unsetf(ios::left);

    cout<<setiosflags(ios::right)<<setw(3)<<1<<endl;//right长期作用
    cout<<resetiosflags(ios::right);

    cout<<setfill('@')<<setw(3)<<1<<endl;//setfill长期作用
    cout.fill(' ');//还原默认

    cout<<setprecision(3)<<3.14159<<endl;//setprecision长期作用
    cout.precision(6);//还原默认
    
    cout<<31.415926<<endl;
    cout<<endl;

    //C++输入补充
    cin.open("Lines.txt");
    char l=cin.get();//读一个字符，包括空格与回车

    for(int o=0;o<3;o++)
    {
        string p;
        getline(cin,p);//会处理回车，但不会读进来
        cout<<p<<endl;
    }
    return 0;
}