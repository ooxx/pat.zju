#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>

using namespace std;

struct Move
{
    int S;
    int E;
};

int main()
{
    ifstream cin("1050.hdu.txt");
    
    int T;
    cin>>T;
    while(T--)
    {
        vector<int> corridor(201,0);
        
        int N;
        cin>>N;
        vector<Move> moves(N);
        for(int i=0;i<N;i++)
        {
            cin>>moves[i].S;
            cin>>moves[i].E;

            moves[i].S=(moves[i].S+1)/2;
            moves[i].E=(moves[i].E+1)/2;

            if(moves[i].S>moves[i].E)
            {
                swap(moves[i].S,moves[i].E);
            }

            for(int j=moves[i].S;j<=moves[i].E;j++)
            {
                corridor[j]++;
            }


        }
        int maxCorridor=0;
        for(int i=0;i<corridor.size();i++)
        {
            if(corridor[i]>maxCorridor)
            {
                maxCorridor=corridor[i];
            }
        }
        cout<<maxCorridor*10<<endl;
    }
    return 0;
}

//------------------------------------------------------------------------------------
// 注意S<E的情况，猥琐的案例中恰好没有出现
// swap(A,B);
//------------------------------------------------------------------------------------