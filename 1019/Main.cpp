#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <iomanip>

#include <vector>
#include <algorithm>

using namespace std;

vector<int> ToRadixFormat(long long value, long long radix)
{
    vector<int> result;
    while(1)
    {
        if(value<radix)
        {
            result.insert(result.begin(),value);
            break;
        }
        result.insert(result.begin(),value%radix);
        value/=radix;
    }
    return result;
}

int main()
{
    long long N,b;
    cin>>N>>b;
    vector<int> result=ToRadixFormat(N, b);
    
    vector<int> rresult=result;
    reverse(rresult.begin(),rresult.end());
    bool equal=true;
    for(int i=0;i<result.size();i++)
    {
        if(result[i] != rresult[i])
            equal=false;
    }
    if(equal == true)
    {
        cout<<"Yes"<<endl;
        for(int i=0;i<result.size();i++)
        {
            if(i != 0)
                cout<<" ";
            cout<<result[i];
        }
    }
    else
    {
        cout<<"No"<<endl;
        for(int i=0;i<result.size();i++)
        {
            if(i != 0)
                cout<<" ";
            cout<<result[i];
        }
    }
    return 0;
}