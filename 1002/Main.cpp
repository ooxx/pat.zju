#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <map>

using namespace std;

int main()
{
    ifstream cin("1002.txt");

    map<int, float> polynomial;
    int K1;
    int K2;
    int exp;
    float coe;
    cin>>K1;
    for(int i=0;i<K1;i++)
    {
        cin>>exp;
        cin>>coe;
        if(polynomial.find(exp)!=polynomial.end())//�Ѵ���
        {
            polynomial[exp]+=coe;
        }
        else
        {
            polynomial[exp]=coe;
        }
    }
    cin>>K2;
    for(int i=0;i<K2;i++)
    {
        cin>>exp;
        cin>>coe;
        if(polynomial.find(exp)!=polynomial.end())//�Ѵ���
        {
            polynomial[exp]+=coe;
        }
        else
        {
            polynomial[exp]=coe;
        }
    }
    cout<<polynomial.size();
    
    for(map<int, float>::reverse_iterator it=polynomial.rbegin();it!=polynomial.rend();it++)
    {
        cout<<" "<<(*it).first<<" "<<setprecision(1)<<fixed<<(*it).second;
    }

    return 0;
}
