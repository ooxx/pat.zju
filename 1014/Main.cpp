#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>

using namespace std;

struct Customer
{
    int Index;
    int ProcessingTime;
    int WaitingTime;
    int LeaveTime;
    bool AlreadyStart;
};

struct Window
{
    vector<Customer*> Customers;
};

struct Queue
{
    vector<Customer*> Customers;
};

int main()
{
    ifstream cin("1014.txt");
    
    int N,M,K,Q;
    cin>>N>>M>>K>>Q;
    vector<Window> windows(N);
    vector<Customer> customers(K);
    Queue queue;
    for(int i=0;i<K;i++)
    {
        customers[i].Index=i;
        cin>>customers[i].ProcessingTime;
        customers[i].WaitingTime=0;
        customers[i].AlreadyStart=false;
    }

    int customerIndex=0;
    int windowIndex=0;
    while(1)
    {
        if(customerIndex == customers.size())
            break;
        if(windows[windows.size()-1].Customers.size() == M)
            break;

        windows[windowIndex].Customers.push_back(&customers[customerIndex]);
        customerIndex++;
        windowIndex++;
        if(windowIndex == windows.size())
            windowIndex=0;
    }
    for(;customerIndex<customers.size();customerIndex++)
    {
        queue.Customers.push_back(&customers[customerIndex]);
    }

    for(int time=1;time<=540;time++)
    {
        //cout<<"Time: "<<time<<endl;
        int emptyChair=0;

        //Windows
        for(int i=0;i<windows.size();i++)
        {
            if(windows[i].Customers.size()==0)
                continue;

            //用以区分是否开始
            if(windows[i].Customers[0]->AlreadyStart == false)
            {
                windows[i].Customers[0]->AlreadyStart=true;
            }
            windows[i].Customers[0]->ProcessingTime--;

            for(int j=0;j<windows[i].Customers.size();j++)
            {
                windows[i].Customers[j]->WaitingTime++;
                if(windows[i].Customers[j]->ProcessingTime == 0)
                {
                    windows[i].Customers[j]->LeaveTime=time;
                    //cout<<i<<"窗"<<windows[i].Customers[j]->Index<<"客户走了"<<endl;
                    windows[i].Customers.erase(windows[i].Customers.begin()+j);
                    emptyChair++;
                }
            }
        }

        //Queue
        for(int i=0;i<queue.Customers.size();i++)
        {
            queue.Customers[i]->WaitingTime++;
        }

        //Queue进入Window
        if(queue.Customers.size() == 0)
            continue;
        for(int i=0;i<emptyChair;i++)
        {
            int windowIndex;
            for(windowIndex=0;windowIndex<windows.size();windowIndex++)
            {
                if(windows[windowIndex].Customers.size() < M)
                {
                    break;
                }
            }
            windows[windowIndex].Customers.push_back(queue.Customers[0]);
            //cout<<queue.Customers[0]->Index<<"客户从队伍进入第"<<windowIndex<<"窗口"<<endl;
            queue.Customers.erase(queue.Customers.begin());
        }

    }

    //17:00后，若正在处理的客户还没完成，则继续处理直至完成。
    for(int i=0;i<customers.size();i++)
    {
        if(customers[i].AlreadyStart==true && customers[i].ProcessingTime!=0)
        {
            customers[i].LeaveTime=540+customers[i].ProcessingTime;
            customers[i].ProcessingTime=0;
        }
    }

    for(int i=0;i<Q;i++)
    {
        int index;
        cin>>index;
        index--;
        if(customers[index].ProcessingTime == 0)
        {
            int hour=8+customers[index].LeaveTime/60;
            int min=customers[index].LeaveTime%60;
            cout<<setw(2)<<setfill('0')<<hour<<":"<<setw(2)<<setfill('0')<<min<<endl;
        }
        else
        {
            cout<<"Sorry"<<endl;
        }
    }
    return 0;
}