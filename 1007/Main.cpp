#define 动态规划
#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>

#define INF (0x6FFFFFFF)
using namespace std;

#ifdef 暴力法
int main()
{
    ifstream cin("1007.txt");

    int K;
    cin>>K;
    vector<int> ns(K);
    int tempSum=0;
    int maxSum=-INF;
    int theMaxHead=0;
    int theMaxFoot=0;
    for(int i=0;i<K;i++)
    {
        cin>>ns[i];
    }

    bool allNeg=true;
    for(int i=0;i<K;i++)
    {
        if(ns[i]>=0)
        {
            allNeg=false;
        }
    }
    if(allNeg==true)
    {
        cout<<"0 "<<ns[0]<<" "<<ns[K-1]<<endl;
        return 0;
    }

    for(int i=0;i<K;i++)
    {
        tempSum=0;
        for(int j=i;j<K;j++)
        {
            tempSum+=ns[j];
            if(tempSum>maxSum)
            {
                maxSum=tempSum;
                theMaxHead=i;
                theMaxFoot=j;
            }
            else if(tempSum==maxSum)
            {
                if(i+j<theMaxHead+theMaxFoot)
                {
                    theMaxHead=i;
                    theMaxFoot=j;
                }
            }
        }
    }
    cout<<maxSum<<" "<<ns[theMaxHead]<<" "<<ns[theMaxFoot]<<endl;
    return 0;
}
#endif
#ifdef 动态规划
int main()
{
    ifstream cin("1007.txt");

    int K;
    cin>>K;
    vector<int> ns(K);
    bool allNeg=true;
    for(int i=0;i<K;i++)
    {
        cin>>ns[i];
        if(ns[i]>=0)
        {
            allNeg=false;
        }
    }
    if(allNeg==true)
    {
        cout<<"0 "<<ns[0]<<" "<<ns[K-1]<<endl;
        return 0;
    }

    int maxSum=-INF;
    int tempSum=0;
    int nextBegin=0;
    int theMaxHead=0;
    int theMaxFoot=0;
    for(int i=0;i<K;i++)
    {
        tempSum+=ns[i];
        if(tempSum>maxSum)
        {
            maxSum=tempSum;
            theMaxHead=nextBegin;
            theMaxFoot=i;
        }
        if(tempSum<0)
        {
            tempSum=0;
            nextBegin=i+1;
        }
    }
    cout<<maxSum<<" "<<ns[theMaxHead]<<" "<<ns[theMaxFoot]<<endl;
    return 0;
}
#endif
