#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>
#include <algorithm>

using namespace std;

struct Student
{
    string Name;
    string ID;
    int Grade;
};

bool CompareF(const Student& s1, const Student& s2)
{
    return s1.Grade>s2.Grade;
}

bool CompareM(const Student& s1, const Student& s2)
{
    return s1.Grade<s2.Grade;
}

int main()
{
    ifstream cin("1036.txt");
    
    int N;
    cin>>N;
    vector<Student> M;
    vector<Student> F;
    for(int i=0;i<N;i++)
    {
        Student s;
        string gender;
        cin>>s.Name>>gender>>s.ID>>s.Grade;
        gender=="M" ? M.push_back(s) : F.push_back(s);
    }

    bool isAbsent=false;
    if(F.size() == 0)
    {
        isAbsent=true;
        cout<<"Absent"<<endl;
    }
    else
    {
        partial_sort(F.begin(),F.begin()+0,F.end(),CompareF);
        cout<<F[0].Name<<" "<<F[0].ID<<endl;
    }
    if(M.size() == 0)
    {
        isAbsent=true;
        cout<<"Absent"<<endl;
    }
    else
    {
        partial_sort(M.begin(),M.begin()+0,M.end(),CompareM);
        cout<<M[0].Name<<" "<<M[0].ID<<endl;
    }
    if(isAbsent == true)
    {
        cout<<"NA"<<endl;
    }
    else
    {
        cout<<F[0].Grade-M[0].Grade<<endl;
    }

    return 0;
}
