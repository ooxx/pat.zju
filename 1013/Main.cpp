#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>
#include <stack>


using namespace std;

struct City
{
    int Index;
    bool Choosen;
    vector<City*> Neighbours;
};

struct Set
{
    vector<City*> Content;

    bool Find(int index)
    {
        bool flag=false;
        for(int i=0;i<Content.size();i++)
        {
            if((*Content[i]).Index==index)
            {
                flag=true;
            }
        }
        return flag;
    }

    void Print()
    {
        cout<<"   (";
        for(int i=0;i<Content.size();i++)
        {
            if(i != 0)
            {
                cout<<" ";
            }
            cout<<Content[i]->Index;
        }
        cout<<")"<<endl;
    }
};

struct Info
{
    int a;
    int b;
};

int main()
{
    ifstream cin("1013.txt");

    int N;
    int M;
    int K;
    cin>>N>>M>>K;

    vector<Info> infos(M);
    for(int i=0;i<M;i++)
    {
        cin>>infos[i].a>>infos[i].b;
    }

    for(int k=0;k<K;k++)
    {
        vector<City> citys(N);
        vector<Set> sets;

        for(int i=0;i<N;i++)
        {
            citys[i].Index=i;
            citys[i].Choosen=false;
        }

        int a,b,c;

        cin>>c;
        c--;

        for(int i=0;i<M;i++)
        {
            a=infos[i].a-1;
            b=infos[i].b-1;
            if((a!=c) && (b!=c))
            {
                citys[a].Neighbours.push_back(&citys[b]);
                citys[b].Neighbours.push_back(&citys[a]);
            }
        }
        for(int i=0;i<N;i++)
        {
            if(citys[i].Choosen==false)
            {
                //cout<<"第"<<i<<"点被检测出还未选择，简历新簇和新栈"<<endl;
                Set aset;
                stack<City*> astack;

                //cout<<".....栈中加入第"<<citys[i].Index<<"点";
                astack.push(&citys[i]);
                //cout<<"当前栈中有"<<astack.size()<<"个元素"<<endl;

                while(1)
                {
                    //检测跳出条件
                    if(astack.empty() == true)
                    {
                        //cout<<"跳出"<<endl;
                        break;
                    }

                    //出栈并处理（标记已读、加入簇）
                    //<<".....栈中弹出第"<<astack.top()->Index<<"点";
                    City* acity=astack.top();astack.pop();
                    //cout<<"当前栈中有"<<astack.size()<<"个元素"<<endl;
                    acity->Choosen=true;
                    if(aset.Find(acity->Index) == false)
                    {
                        //cout<<".....簇中加入第"<<acity->Index<<"点";
                        aset.Content.push_back(acity);
                        //cout<<"当前簇中内容为";aset.Print();
                    }

                    //入栈所有相关的、还没被Choosen过的项
                    for(int j=0;j<acity->Neighbours.size();j++)
                    {
                        if(acity->Neighbours[j]->Choosen==false)
                        {
                            //cout<<".....栈中加入第"<<acity->Neighbours[j]->Index<<"点";
                            astack.push(acity->Neighbours[j]);
                            //cout<<"当前栈中有"<<astack.size()<<"个元素"<<endl;
                        }
                    }
                }

                //cout<<"簇闭合"<<"当前簇中内容为   ";aset.Print();cout<<endl;
                sets.push_back(aset);
            }
        }
        //cout<<"簇的数量为"<<sets.size()<<endl;
        cout<<sets.size()-2<<endl;
    }

    return 0;
}