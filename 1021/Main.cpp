#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <vector>
#include <queue>

#include <stdio.h>

using namespace std;

struct Node
{
    int Index;
    vector<Node*> Neighbours;
};

struct Set
{
    vector<Node*> Content;

    void Print()
    {
        cout<<"   (";
        for(int i=0;i<Content.size();i++)
        {
            if(i != 0)
            {
                cout<<" ";
            }
            cout<<Content[i]->Index;
        }
        cout<<")"<<endl;
    }
};

int BFS(vector<Node> nodes)
{
    vector<Set> sets;

    vector<bool> choosen(nodes.size(),false);

    for(int i=0;i<nodes.size();i++)
    {
        if(choosen[nodes[i].Index]==false)
        {
            Set aset;
            queue<Node*> aqueue;

            choosen[nodes[i].Index]=true;
            aqueue.push(&nodes[i]);

            while(1)
            {
                //检测跳出条件
                if(aqueue.empty() == true)
                    break;

                //出栈并加入集合
                Node* anode=aqueue.front();aqueue.pop();
                aset.Content.push_back(anode);

                //入栈所有相关的、还没被Choosen过的项
                for(int j=0;j<anode->Neighbours.size();j++)
                {
                    if(choosen[anode->Neighbours[j]->Index]==false)
                    {
                        choosen[anode->Neighbours[j]->Index]=true;
                        aqueue.push(anode->Neighbours[j]);
                    }
                }
            }

            sets.push_back(aset);
        }
    }

    return sets.size();
}

int BFSRootDeepth(vector<Node> nodes, int startIndex)
{
    queue<Node*> aqueue;
    vector<bool> choosen(nodes.size(),false);
    choosen[nodes[startIndex].Index]=true;
    aqueue.push(&nodes[startIndex]);
    int deepth=0;

    while(1)
    {
        if(aqueue.size() == 0)
            break;

        deepth++;

        queue<Node*> bqueue;
        while(aqueue.size() != 0)
        {
            Node* target=aqueue.front();aqueue.pop();
            for(int i=0;i<target->Neighbours.size();i++)
            {
                if(choosen[target->Neighbours[i]->Index] == false)
                {
                    choosen[target->Neighbours[i]->Index]=true;
                    bqueue.push(target->Neighbours[i]);
                }
            }
        }
        aqueue=bqueue;
    }

    return deepth;
}

int main()
{
    freopen("1021.txt","r",stdin);//ifstream cin("1021.txt");

    int N;
    scanf("%d", &N);//cin>>N;

    vector<Node> nodes(N);
    vector<int> length(N);

    for(int i=0;i<N;i++)
    {
        nodes[i].Index=i;
    }
    for(int i=0;i<N-1;i++)
    {
        int a,b;
        scanf("%d %d", &a, &b);//cin>>a>>b;
        a--;
        b--;
        nodes[a].Neighbours.push_back(&nodes[b]);
        nodes[b].Neighbours.push_back(&nodes[a]);
    }

    int asize=BFS(nodes);
    if(asize != 1)
    {
        printf("Error: %d components\n",asize);//cout<<"Error: "<<asize<<" components"<<endl;
        return 0;
    }

    int maxLength=0;
    for(int i=0;i<N;i++)
    {
        length[i]=BFSRootDeepth(nodes,i);
        if(length[i]>maxLength)
        {
            maxLength=length[i];
        }
    }

    for(int i=0;i<N;i++)
    {
        if(length[i] == maxLength)
        {
            printf("%d\n", i+1);//cout<<i+1<<endl;
        }
    }

    return 0;
}
