#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>

#define INF 0x7FFFFFFF

using namespace std;

struct Player
{
    int ID;
    int ArrivingTime;
    int WaitingTime;
    int ServingTime;
    int LeavingTime;
    int PlayingTime;
    bool VIP;
};

struct Table
{
    int ID;
    bool Occupied;
    int ContinueTime;
    vector<Player*> ServedPlayers;
    bool VIP;
};

int ToDigit(char ch)
{
    return ch-48;
}

void PrintTime(int time)
{
    int hour=time/3600;
    int mins=(time%3600)/60;
    int seds=time%60;
    cout<<setw(2)<<setfill('0')<<hour<<":";
    cout<<setw(2)<<setfill('0')<<mins<<":";
    cout<<setw(2)<<setfill('0')<<seds;
}

bool CompareByArrive(const Player* p1, const Player* p2)
{
    return p1->ArrivingTime<p2->ArrivingTime;
}

bool CompareByServe(const Player& p1, const Player& p2)
{
    return p1.ServingTime<p2.ServingTime;
}

int main()
{
    ifstream cin("1026.txt");

    int N;
    cin>>N;
    vector<Player> players(N);
    for(int i=0;i<N;i++)
    {
        string time;
        int vip;
        cin>>time>>players[i].PlayingTime>>vip;
        int hour=ToDigit(time[0])*10+ToDigit(time[1]);
        int mins=ToDigit(time[3])*10+ToDigit(time[4]);
        int seds=ToDigit(time[6])*10+ToDigit(time[7]);
        players[i].ArrivingTime=hour*3600+mins*60+seds;
        players[i].ID=i;
        players[i].PlayingTime*=60;
        players[i].WaitingTime=0;
        players[i].ServingTime=INF;
        vip==1 ? players[i].VIP=true : players[i].VIP=false;
    }
    int K,M;
    cin>>K>>M;
    vector<Table> tables(K);
    for(int i=0;i<K;i++)
    {
        tables[i].ID=i;
        tables[i].Occupied=false;
        tables[i].ContinueTime=0;
        tables[i].VIP=false;
    }
    for(int i=0;i<M;i++)
    {
        int index;
        cin>>index;
        tables[index-1].VIP=true;
    }

    vector<Player*> queue;
    vector<Player*> door;
    for(int i=0;i<players.size();i++)
    {
        door.push_back(&players[i]);
    }
    sort(door.begin(),door.end(),CompareByArrive);

    for(int time=8*3600;time<21*3600;time++)
    {
        //PrintTime(time);
        
        //tables
        vector<Table*> emptyNonvipTable;
        vector<Table*> emptyVipTable;
        for(int i=0;i<tables.size();i++)
        {
            if(tables[i].Occupied == false)
            {
                //cout<<"      Table - Table "<<i<<"为空"<<endl;
                continue;
            }

            //cout<<"      Table - Table "<<i<<"不为空，Player"<<tables[i].ServedPlayers[tables[i].ServedPlayers.size()-1]->ID<<"正在使用"<<endl;
            int last=tables[i].ServedPlayers.size()-1;
            tables[i].ContinueTime++;
            if(tables[i].ServedPlayers[last]->PlayingTime == 2*3600)
            {
                //while(1);// for pat debug 
                tables[i].ContinueTime=0;
                tables[i].Occupied=false;
                if(tables[i].VIP == true)
                {
                    emptyVipTable.push_back(&tables[i]);
                }
                else
                {
                    emptyNonvipTable.push_back(&tables[i]);
                }
            }
            if(tables[i].ServedPlayers[last]->PlayingTime == tables[i].ContinueTime)
            {
                //cout<<"      Table - Player "<<tables[i].ServedPlayers[tables[i].ServedPlayers.size()-1]->ID<<"从Table "<<i<<"退出"<<endl;
                tables[i].ContinueTime=0;
                tables[i].Occupied=false;
                if(tables[i].VIP == true)
                {
                    emptyVipTable.push_back(&tables[i]);
                }
                else
                {
                    emptyNonvipTable.push_back(&tables[i]);
                }
            }
        }

        //queue
        vector<Player*> vipInQueue;
        for(int i=0;i<queue.size();i++)
        {
            queue[i]->WaitingTime++;
            if(queue[i]->VIP == true)
            {
                vipInQueue.push_back(queue[i]);
            }
        }
        //遍历，占满所有空的table
        while(1)
        {
            if(emptyNonvipTable.size()==0 && emptyVipTable.size()==0)
                break;
            if(queue.size()==0)
                break;

            //有vip table，且queue里有vip
            if(vipInQueue.size()!=0 && emptyVipTable.size()!=0)
            {
                //cout<<"      Queue - Player "<<vipInQueue[0]->ID<<"加入Table "<<emptyVipTable[0]->ID<<endl;
                emptyVipTable[0]->ServedPlayers.push_back(vipInQueue[0]);
                emptyVipTable[0]->ContinueTime=0;
                emptyVipTable[0]->Occupied=true;
                emptyVipTable.erase(emptyVipTable.begin());

                vipInQueue[0]->ServingTime=time;
                int toDeleteId=vipInQueue[0]->ID;
                vipInQueue.erase(vipInQueue.begin());
                //删queue内同一个指针
                for(int i=0;i<queue.size();i++)
                {
                    if(queue[i]->ID == toDeleteId)
                    {
                        queue.erase(queue.begin()+i);
                        break;
                    }
                }
            }
            //无需考虑vip的情况
            else
            {
                if(emptyNonvipTable.size()==0)
                {
                    //cout<<"      Queue - Player "<<queue[0]->ID<<"加入Table "<<emptyVipTable[0]->ID<<endl;
                    emptyVipTable[0]->ServedPlayers.push_back(queue[0]);
                    emptyVipTable[0]->ContinueTime=0;
                    emptyVipTable[0]->Occupied=true;
                    emptyVipTable.erase(emptyVipTable.begin());
                    queue[0]->ServingTime=time;
                    queue.erase(queue.begin());
                }
                else if(emptyVipTable.size()==0)
                {
                    //cout<<"      Queue - Player "<<queue[0]->ID<<"加入Table "<<emptyNonvipTable[0]->ID<<endl;
                    emptyNonvipTable[0]->ServedPlayers.push_back(queue[0]);
                    emptyNonvipTable[0]->ContinueTime=0;
                    emptyNonvipTable[0]->Occupied=true;
                    emptyNonvipTable.erase(emptyNonvipTable.begin());
                    queue[0]->ServingTime=time;
                    queue.erase(queue.begin());
                }
                else
                {
                    if(emptyNonvipTable[0]->ID < emptyVipTable[0]->ID)
                    {
                        //cout<<"      Queue - Player "<<queue[0]->ID<<"加入Table "<<emptyNonvipTable[0]->ID<<endl;
                        emptyNonvipTable[0]->ServedPlayers.push_back(queue[0]);
                        emptyNonvipTable[0]->ContinueTime=0;
                        emptyNonvipTable[0]->Occupied=true;
                        emptyNonvipTable.erase(emptyNonvipTable.begin());
                        queue[0]->ServingTime=time;
                        queue.erase(queue.begin());
                    }
                    else
                    {
                        //cout<<"      Queue - Player "<<queue[0]->ID<<"加入Table "<<emptyVipTable[0]->ID<<endl;
                        emptyVipTable[0]->ServedPlayers.push_back(queue[0]);
                        emptyVipTable[0]->ContinueTime=0;
                        emptyVipTable[0]->Occupied=true;
                        emptyVipTable.erase(emptyVipTable.begin());
                        queue[0]->ServingTime=time;
                        queue.erase(queue.begin());
                    }
                }
            }
        }

        //door
        if(door.size() == 0)
            continue;
        if(door[0]->ArrivingTime==time)
        {
            //queue空，检查进tables
            if(queue.size()==0)
            {
                //遍历出第一个空的table
                int index=-1;
                for(int i=0;i<tables.size();i++)
                {
                    if(tables[i].Occupied == false)
                    {
                        index=i;
                        break;
                    }
                }
                //table有空，加入table
                if(index != -1)
                {
                    //cout<<"      Door - Player "<<door[0]->ID<<"加入Table "<<tables[index].ID<<endl;
                    tables[index].ServedPlayers.push_back(door[0]);
                    tables[index].ContinueTime=0;
                    tables[index].Occupied=true;
                    door[0]->ServingTime=time;
                    door.erase(door.begin());
                }
                //table没空，加入queue
                else
                {
                    //cout<<"      Door - Player "<<door[0]->ID<<"加入queue"<<endl;
                    queue.push_back(door[0]);
                    door.erase(door.begin());
                }
            }
            //queue不空，直接挂queue后
            else
            {
                //cout<<"      Door - Player "<<door[0]->ID<<"加入queue"<<endl;
                queue.push_back(door[0]);
                door.erase(door.begin());
            }
        }
    }//for time

    sort(players.begin(),players.end(),CompareByServe);
    for(int i=0;i<players.size();i++)
    {
        if(players[i].ServingTime>=21*3600)
            continue;
        PrintTime(players[i].ArrivingTime);
        cout<<" ";
        PrintTime(players[i].ServingTime);
        cout<<" "<<(players[i].WaitingTime+30)/60<<endl;
    }

    for(int i=0;i<tables.size();i++)
    {
        if(i != 0)
            cout<<" ";
        cout<<tables[i].ServedPlayers.size();
    }

    return 0;
}
