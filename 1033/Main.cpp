#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;

struct Node
{
    float GasPrice;
    float GasBought;
    float TotalDistance;
    float DistanceToNext;
    float UsedQuota;
};

bool NodesCompare(const Node& node1, const Node& node2)
{
    return node1.TotalDistance<node2.TotalDistance;
}

int SearchAvaliableCheapest(vector<Node> nodes, float Quota, int i)
{
    int beginIndex=-1;
    for(int j=0;j<i;j++)
    {
        if(abs(nodes[j].UsedQuota-Quota)<=0.01)//nodes[j].UsedQuota==Quota
        {
            beginIndex=j;
        }
    }
    int cheapestIndex=beginIndex+1;
    for(int j=beginIndex+1;j<=i;j++)
    {
        if(nodes[j].GasPrice<nodes[cheapestIndex].GasPrice)
        {
            cheapestIndex=j;
        }
    }
    return cheapestIndex;
}

int main()
{
    ifstream cin("1033.txt");

    float Cmax;
    float D;
    float Davg;
    int N;
    cin>>Cmax>>D>>Davg>>N;
    vector<Node> nodes(N);
    float Quota=Cmax*Davg;
    bool errorFlag=false;
    for(int i=0;i<N;i++)
    {
        cin>>nodes[i].GasPrice;
        nodes[i].GasBought=0;
        cin>>nodes[i].TotalDistance;
        nodes[i].UsedQuota=0;
    }
    sort(nodes.begin(),nodes.end(),NodesCompare);
    for(int i=0;i<N-1;i++)
    {
        nodes[i].DistanceToNext=nodes[i+1].TotalDistance-nodes[i].TotalDistance;
    }
    nodes[N-1].DistanceToNext=D-nodes[N-1].TotalDistance;

    for(int i=0;i<N;i++)
    {
        float quotaNeeded=nodes[i].DistanceToNext;
        while(1)
        {
            int index=SearchAvaliableCheapest(nodes,Quota,i);
            int usedThisTime;
            if(Quota-nodes[index].UsedQuota>=quotaNeeded)//够用了
            {
                usedThisTime=quotaNeeded;
                quotaNeeded-=usedThisTime;
                nodes[index].UsedQuota+=usedThisTime;
                for(int j=index+1;j<=i;j++)
                {
                    nodes[j].UsedQuota+=usedThisTime;
                }
                nodes[index].GasBought+=usedThisTime/Davg;

                break;
            }
            else//还不够用
            {
                usedThisTime=Quota-nodes[index].UsedQuota;
                if(usedThisTime == 0)
                {
                    errorFlag=true;
                    break;
                }
                quotaNeeded-=usedThisTime;
                nodes[index].UsedQuota+=usedThisTime;
                for(int j=index+1;j<=i;j++)
                {
                    nodes[j].UsedQuota+=usedThisTime;
                }
                nodes[index].GasBought+=usedThisTime/Davg;
            }
        }
        /*
        cout<<"第"<<i<<"到"<<i+1<<"段完成，各点使用配额情况"<<endl;
        for(int j=0;j<N;j++)
        {
        cout<<j<<": "<<nodes[j].UsedQuota<<" "<<nodes[j].GasBought<<endl;
        }
        cout<<endl;
        */
    }
    if(errorFlag==false)
    {
        float totalPrice=0;
        for(int i=0;i<N;i++)
        {
            totalPrice+=nodes[i].GasPrice*nodes[i].GasBought;
        }
        cout<<setprecision(2)<<fixed<<totalPrice;
    }
    else
    {
        float totalQuota=0;
        for(int i=0;i<N;i++)
        {
            totalQuota+=nodes[i].UsedQuota;
        }
        cout<<"The maximum travel distance = "<<setprecision(2)<<fixed<<totalQuota;
    }
    return 0;
}