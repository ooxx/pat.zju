#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
#include <algorithm>

#include <stdio.h>

using namespace std;

bool Compare(const int& i1, const int& i2)
{
    return i1>i2;
}

int main()
{
    //ifstream cin("1037.txt");
    freopen("1037.txt","r",stdin);

    int NC,NP;
    scanf("%d",&NC);//cin>>NC;
    vector<int> coupons(NC);
    for(int i=0;i<NC;i++)
    {
        scanf("%d",&coupons[i]);//cin>>coupons[i];
    }
    sort(coupons.begin(),coupons.end(),Compare);

    cin>>NP;
    vector<int> products(NP);
    for(int i=0;i<NP;i++)
    {
        scanf("%d",&products[i]);//cin>>products[i];
    }
    sort(products.begin(),products.end(),Compare);

    int getBack=0;
    /*
    while(1)
    {
    if(coupons.size() == 0)
    break;
    if(coupons[coupons.size()-1]>=0)
    break;

    if(products[products.size()-1]<0)
    {
    getBack+=coupons[coupons.size()-1]*products[products.size()-1];
    products.erase(products.end()-1);
    }
    coupons.erase(coupons.end()-1);
    }
    for(int i=0;i<coupons.size();i++)
    {
    if(products.size() == 0)
    break;

    if(products[0]>0)
    {
    getBack+=coupons[i]*products[0];
    products.erase(products.begin());
    }
    }
    */
    for(int i = 0; i < coupons.size() && i < products.size() ; ++i )
    {
        if(coupons[i] > 0 && products[i] > 0)
            getBack += coupons[i]*products[i];
        else
            break;
    }
    for(int i = coupons.size()-1, j = products.size()-1; i >= 0 && j >= 0  ; --i,--j )
    {
        if(coupons[i] < 0 && products[j] < 0)
            getBack += coupons[i]*products[j];
        else
            break;
    }

    printf("%d\n",getBack);//cout<<getBack<<endl;

    return 0;
}