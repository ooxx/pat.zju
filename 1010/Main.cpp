#include <iostream>
#include <sstream>
#include <fstream>

#include <string>
#include <iomanip>

#include <cmath>

using namespace std;

int ToInt(char ch)
{
    if(ch>='0' && ch<='9')
    {
        return ch-48;
    }
    else
    {
        return ch-87;
    }
}

int main()
{
    ifstream cin("1010.txt");

    string N1,N2;
    int tag,radix;

    long long value;
    string symbol;

    cin>>N1>>N2>>tag>>radix;
    if(tag==1)
    {
        value=0;
        symbol=N2;
        for(int i=0;i<N1.size();i++)
        {
            value+=((double)(ToInt(N1[i])))*(pow((double)radix,(double)(N1.size()-i-1)));
        }
    }
    else
    {
        value=0;
        symbol=N1;
        for(int i=0;i<N2.size();i++)
        {
            value+=((double)(ToInt(N2[i])))*(pow((double)radix,(double)(N2.size()-i-1)));
        }
    }
    for(int i=2;i<=36;i++)
    {
        long long totalValue=0;
        for(int j=0;j<symbol.size();j++)
        {
            totalValue+=((double)(ToInt(symbol[j])))*(pow((double)i,(double)(symbol.size()-j-1)));
        }
        if(totalValue==value)
        {
            cout<<i;
            return 0;
        }
    }
    cout<<"Impossible";
    return 0;
}