#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <iomanip>

#include <vector>
#include <algorithm>

#define MAXV (510)
#define INF (0x6FFFFFFF)
using namespace std;

struct Node
{
    int dist;
    bool known;
    vector< vector<int> > Paths;

    void Clear(int i)
    {
        dist = INF;//本点到起始点的距离
        known = false;//一开始，所有点都未加入选取集合，标记为未知
        Paths.clear();
    }

    void Print(int i)
    {
        cout<<setw(3)<<i;
        if(dist == INF)
        {
            cout<<"     总距离INF";
        }
        else
        {
            cout<<"     总距离"<<setw(3)<<dist;
        }

        cout<<"     路径数"<<setw(3)<<Paths.size()<<"     ";

        if(known==false)
        {
            cout<<"unknown     ";
        }
        else
        {
            cout<<"  known     ";
        }

        for(int i=0;i<Paths.size();i++)
        {
            if(i != 0)
            {
                cout<<" / ";
            }
            for(int j=0;j<Paths[i].size();j++)
            {
                if(j !=0)
                {
                    cout<<">";
                }
                cout<<Paths[i][j];
            }

        }
        cout<<endl;
    }
};

Node city[MAXV];
int adj[MAXV][MAXV];
int N,M;

/*------------------------------------------------------------------------------------
 Dijkstra和Prim共同思路
 
 初始化
 while(1)
 {
    找出最“近”点
    标记最“近”点
    更新临近最“近”点的各点的信息
 }
------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------
Dij思路
void dijkstra(start end)
{
    初始化city[start]数据;
    while(1)
    {
        nearest_unknown=-1
        查找所有未知点中，总距离最短的点nearest_unknown;
        if(nearest_unknown == -1)
        {
            break;
        }
        city[nearest_unknown].known = true;
        for(所有点i）
        {
            if(nearest_unknown和i相邻 && i未知)
            {
                if(起点到nearest_unknown + nearest_unknown到i < 起点直接到i)
                {
                    更新Paths;（沿用前一点的Paths，在最后加上i的信息）
                    更新call等;
                }
                else if(==)
                {
                    更新Paths;（新建一条path，将来自nearest_unknown的信息拷贝添加到path上，并加上i的信息，加入Paths）
                    更新call等;
                }
            }
        }
    }
}
------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------
Prime思路
void Prim(start)
{
    初始化所有city[i].dist数据;
    while(1)
    {
        nearest_unknown=-1
        查找所有未知点中，距离集合最短的点nearest_unknown;
        if(nearest_unknown == -1)
        {
            break;
        }
        city[nearest_unknown].known = true;
        for(所有点i）
        {
            if(nearest_unknown和i相邻 && i未知)
            {
                if(i到nearest_unknown < i到集合)
                {
                    更新city[i].dist;
                }
            }
        }
    }
}
------------------------------------------------------------------------------------*/
int Prim(int start)
{
    int result=-1;
    
    for(int i=0;i<N;i++)
    {
        city[i].dist=adj[start][i];
    }

    while(1)
    {
        cout<<"开始新一轮循环，当前各点情况："<<endl;
        for(int i=0;i<N;i++)
        {
            city[i].Print(i);
        }

        //在所有未知节点中，查找出距离known点集最近的点
        //注意没有起点之分，针对未知点
        //结束这个循环后，nearest_unknown代表总距离known点集最近的未知节点
        //若有几个点，总距离相等，则选择其中一个，选哪个对算法无影响
        //首次执行，nearest_unknown为0
        int nearest_unknown = -1;
        int nearest = INF;
        for(int i=0;i<N;i++)
        {
            if(city[i].known==false && city[i].dist<nearest)
            {
                nearest = city[i].dist;
                nearest_unknown = i;
            }
        }

        if(nearest_unknown == -1)
        {
            cout<<"选取集合已加入所有非孤立点，无未知点，跳出"<<endl;//特殊情况，若有点孤立，则不会加入选取集合，其dist=INF
            break;
        }
        else
        {
            if(nearest > result)
                result=nearest;
        }

        //把距离最近的点加入到选取集合，标记为已知点
        cout<<"找到最近的未知点："<<endl;
        city[nearest_unknown].Print(nearest_unknown);

        city[nearest_unknown].known = true;

        //更新信息
        //遍历所有和最近加入的这个点相邻的未知点，比较它们和这个点的总距离，更新所有未知相邻点的信息
        //如果是第一次入选相邻点，就用真实距离代替INF
        cout<<".....开始更新所有未知相邻点信息....."<<endl;
        for(int i=0;i<N;i++)
        {
            //判断相邻点未知
            if( adj[nearest_unknown][i] != INF && city[i].known == false )
            {
                cout<<"     找到未知相邻点："<<endl<<"     ";
                city[i].Print(i);

                //city[i].dist表示该点到所有known点间的最短距离
                //如果发现因为nearest_unknown的新加入，使city[i]出现了更短的、到所有known点间的距离
                //则更新city[i].dist
                //具体做法，只要将city[i].dist和 i到nearest_unknown间距离比较
                if(adj[i][nearest_unknown] < city[i].dist )
                {
                    city[i].dist=adj[nearest_unknown][i];

                    cout<<"     更新未知相邻点信息，更新后："<<endl<<"     ";
                    city[i].Print(i);
                }
            }
        }//for
        cout<<".....结束更新所有未知相邻点信息....."<<endl;
    }//while
    return result;
}
int main()
{

    ifstream cin("Prim.txt");

    cin>>N;
    for(int i=0;i<N;i++)
    {
        city[i].Clear(i);
    }
    for(int i=0;i<N;i++)
    {
        for(int j=0;j<N;j++)
        {
            if( i == j )
            {
                adj[i][j] = 0;
            }
            else
            {
                adj[i][j] = INF;
            }
        }
    }

    cin>>M;
    while(M--)
    {
        int a,b,c;
        cin>>a>>b>>c;
        adj[a][b]=c;
        adj[b][a]=c;
    }
    cout<<Prim(0)<<endl;
    return 0;
}